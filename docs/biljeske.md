# Organizacija posla

## ~~Izrada library-ja za unit-testiranje~~

## ~~Izrada genetskog algoritma~~

* ~~k-turnirska selekcija~~
* ~~mutacija s dodavanjem slučajne varijable iz gaussove razdiobe~~
	* ~~aproksimacija gaussove razdiobe~~
* ~~zadavanje fitnessa izvana~~
* ~~testiranje na labosu iz APR-a~~

## ~~Izrada neuronske mreže~~

* ~~vanjske težine~~
* ~~minimizacija memorije~~
* ~~aktivacijska funkcija sigma~~
* ~~testiranje usporedbom na papiru~~

## Unificirani test na labosu iz NENR-a ***neuspjelo***

* labos zahtjeva implementaciju druge vrste neurona u prvom skrivenom sloju, možda je tu problem

## ~~Implementacija modela automobila~~

* Ackermannovo upravljanje ***neuspjelo***
	* pretjerana kompliciranost algoritma
* ~~improvozirani model~~
* ~~OpenGL aplikacija za štimanje modela~~

## ~~Zajednički test GA-NN-auto~~

* ~~program za učenje~~
	* ~~najveća euklidska udaljenost u n koraka~~
	* ~~ispis najbolje jedinke u file~~
* ~~program za vizualizaciju~~
	* ~~učitavanje jedinke iz file~~
	* ~~OpenGL simulacija kretnje auta~~

## Implementacija trkaće staze

* proučiti model za sudare sa stazom
* implementacija check-pointova
* vizualizacija staze
* vizualizacija sudara za nasumično kretanje auta po stazi
* testirati računanje udaljenosti točke na stazi

## Implementacija senzora za stazu

* najvjerojatnije 2D raytracing, proučiti još
* testiranje usporedbom s ručno izračunatim primjerom

## Zajednički test GA-NN-auto-staza

* program za učenje
	* najveća prijeđenost staze u n koraka
	* ispis najbolje jedinke u file
* program za vizualizaciju
	* učitavanje jedinke iz filea
	* OpenGL simulacija kretanja auta po stazi

## Modul za zapisivanje/učitavanje postavki/jedinki

* učitavanje kromosoma jedinke za drugu arhitekturu NN
	* dopunjavanje/rezanje vrijednosti kako bi stale iz jedne u drugu arhitekturu NN
* stvaranje populacije iz učitane jedinke za daljnje učenje
* zapisivanje postavki i rezultata učenja radi statistie hiperparametara
* testiranje

## Proširiti model automobila

* nekoliko automobila istovremeno
* implementacija sudara između automobila
* testiranje sudara programom za vizualizaciju i štimanje modela

## Senzori drugih igrača

* smjer i udaljenost
* traženje senzorima
* testiranje efikasnosti oba

## Implementacija složenog kontrolera

* nekoliko kontrolera utječu na upravljanje automobilom
* težina svakog kontrolera kao hiperparametar

## Učenje više kontrolera

* treniranje vozača čiji fitness ovisi isključivo o relativnoj poziciji naspram suparnika
* treniranje kombinacije relativne pozicije i apsolutne prijeđenosi
* program za vizualizaciju utrke više igrača

## Evolucija senzora

* senzor upisan u kromosom, statički kroz utrku
	* potencijalna pretreniranost, stalno gledanje ili lijevo ili desno, ne radi na više staza
* dinamičko pomicanje senzora svaki korak

## Vizualna aplikacija za utrkivanje čovjek-CPU

* odabir staze
* odabir protivnika

## Potencijalno za dodati

Stvari koje bi se mogle dodati ukoliko bude vremena, ali su trenutno van scope-a rada

### GPU ubrzanje

* proučiti implementacije ubrzanja ML algoritama za GPU
* programiranje shader-a u OpenGL-u i GLSL-u

### Pamćenje

* omogućiti učenje uz prikaz ulaza u nekim prethodnim koracima
	* potencijalno razvijanje taktike poput namjernog zabijanja u protivnika

### 3D utrkivanje čovjek-CPU

* 3D prikaz auta, uz 2D fiziku sudara i kretanja
* praćenje auta ljudskog igrača
* upravljanje PS3 upravljačem
