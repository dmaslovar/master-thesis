# Simulacija automobilske utrke korištenjem evolucijskih algoritama

Direktorij `src/` sadrži sve programske kodove razvijene u sklopu projekta, dok direktorij `docs/` sadrži popratnu dokumentaciju.

## docs/ direktorij

Sadrži diplomski.tex koji se može prevesti u pdf i literatura.bib što je literatura diplomski.tex-a. `docs/literature/` sadrži literaturu navedenu u diplomskom radu.

## src/ direktorij

Trenutna struktura `src/` direktorija dana je u nastavku:
 * evol
 * neur
 * util
  * gauss
  * unit_test
  * conf
 * model
 * fit
 * include
 * lib
 * param_search
 * test
 * tracks
Svaki direktorij list sadrži svoj vlastiti testni program za testiranje kodova u tom direktoriju te `makefile` koji ga prevodi. Pretpostavljena direktiva `makefile`-a prevodi testni program u izvršnu datoteku `run_test` i pokreće ju. Svaki direktorij koji sadrži knjižnicu u svom `makefile`-u ima `lib` direktivu koja ju prevodi u dinamički povezanu knjižnicu. Da bi se programi mogli prevoditi, potrebno je preuzeti `freeglut3-dev`, paket dostupan u repozitorijima većine Linux distribucija.

### unit_test

Direktorij koji sadrži knjižnicu `test.c` koja se koristi u ostalim djelovima projekta za automatizirano testiranje pojedinih metoda i njihovih rezultata.

### gauss

Knjižnica `gauss.c` sadrži metodu `double rand_gauss()` koja generira pseudo-nasumične realne brojeve po normalnoj razdiobi te metodu `double rand_scaled()` koja generira pseduo nasumične realne brojeve po uniformnoj razdiobi.

### conf

Pomoćna knjižnica za čitanje i pisanje konfiguraciju u obliku `neko.polje = neka vrijednost`

### evol

Knjižnica `evol_alg.c` sadrži potrebnu podršku za stvaranje evolucijskog algoritma i nasumične populacije te izvođenje učenja. Agoritam je implementacija k-turnirske selekcije koja u kromosom najgore jedinke turnira upisuje vrijednosti iz kromosoma najbolje jedinke, uz dodavanje nasumične vrijednosti iz normalne razdiobe.

### neur

Knjižnica `neur_net.c` sadrži podršku za stvaranje neuronske mreže i unaprijedni prolaz kroz neuronsku mrežu, odnosno određivanje izlaza na temelju ulaza. Neuronska mreža je implementirana na način da koristi vanjski niz realnih brojeva kao težine. Stoga je ovako implementirana neuronska mreža savršena za korištenje u funkciji dobrote evolucijskog algoritma.

### model

Fizikalni model utrke i vozila

### fit

Knjižnica s definiranim funkcijama dobrote. `make` također prevodi izvršne programe `train` i `show`. `train` prima argumente `conf_file` za konfiguracijsku datoteku i `res_file` s imenom datoteke u koju da zapiše rezultat izvođenja. `show` prima ime datoteke s rezultatom treniranja te prikazuje kako model upravlja vozilom na stazama na kojima se trenirao i testirao. Pogledati konfiguracijske datoteke u direktoriju za inspiraciju.

### include

Direktorij s bash skriptom `makelinks.sh` koja stvara simboličke poveznice na header datoteke svih knjižnica. Sva prevođenja izvršnih programa se referiraju na taj direktorij kao mjesto s header datotekama.

### lib

Direktorij s bash skriptom `makelinks.sh` koja stvara simboličke poveznice na dinamički povezane knjižnice i prevodi ih ako ne postoje. `makefile` svake knjižnice ima cilj `lib` koji stvara knjižnicu te se taj mehanizam poziva prilikom prevođenja. Sva prevođenja programa se referiraju na taj direktorij kao mjesto s dinamički povezanim knjižnicama.

### param_search

Direktorij u kojem se nalazi izvršni program za pretragu hiperparametara. Pogledati postojeću konfiguracijsku datoteku za inspiraciju. `result/` direktorij sadrži rezultate prijašnjih pokretanja pretrage hiperparametara. `stats/` sadrži datoteke pripremljene za izradu boxplotova po hiperparametrima, njih je generirao izvršni program `analyse`. `analyse` analizira sve `result.txt` datoteke iz pretrage hiperparametara te grupira rezultate po različitim mogućim vrijednostima hiperparametara. Sam program nema mehanizam za rekurzivno obilaženje direktorija, pa ga je najbolje pozvati s `./analyse conf.txt $(find result -name result.txt)` jer prima popis datoteka s rezultatima

### test

Sadrži poddirektorije s testnim programima. Programi su služili ispitivanjima i sanity-checkovima kroz razvoj te se sada vjerojatno ni ne prevode više.
### tracks

Direktorij sa svim stazama razvijenima u sklopu diplomskog rada.
