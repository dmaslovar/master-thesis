#include <stdio.h>
#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#include <neur_net.h>
#include <model.h>
#include <fitness.h>

#define STEER 0
#define ACCEL 1

#define T_STEP 0.02

#define IND_IN 0
#define IND_OUT 1

struct _fitness_settings {
	int id_len;
	struct track_ids *ids;

	struct neural_net *phase1_net;
	double *phase1_weights;
	int phase1_buf[2];
	double *phase1_in;
	double *phase1_out;

	struct neural_net *phase2_net;
	int phase2_buf[2];
	double *phase2_in;
	double *phase2_out;

	int n_wall_angles;
	int n_car_angles;
	double *wall_angles;
	double *car_angles;

	double alpha;
	double beta;
	int n_steps;
} _fit_set;

struct _test_fitness_settings {
	int id_len;
	struct track_ids *ids;

	struct neural_net *net;
	int buf_len[2];
	double *in;
	double *out;

	int n_wall_angles;
	int n_car_angles;
	double *wall_angles;
	double *car_angles;

	int n_steps;
} _fit_test;

int _fitness_set = 0;
int _fitness_tracks_set = 0;
int _fitness_test_set = 0;
int _fitness_test_tracks_set = 0;

double phase1_fit(int size, double *weights) {
	double fit = 0;
	int car;
	struct track_ids *ids;

	for (int j = 0; j < _fit_set.id_len; j++) {
		ids = _fit_set.ids + j;
		clear_cars(ids->track_id);
		car = init_car(ids->track_id, ids->car_unit);

		for (int i = 0; i < _fit_set.n_steps; i++) {
			_fit_set.phase1_in[0] = get_car_velocity(ids->track_id, car);
			_fit_set.phase1_in[1] = next_chckpt_ang(ids->track_id, car);
			for (int i = 0; i < _fit_set.n_wall_angles; i++)
				_fit_set.phase1_in[i + 2] = get_wall_dist(ids->track_id, car, _fit_set.wall_angles[i]);

			eval_neur_net(_fit_set.phase1_net, weights, _fit_set.phase1_in, _fit_set.phase1_out);
			set_car_input(ids->track_id, car, CAR_STEER, _fit_set.phase1_out[STEER]);
			set_car_input(ids->track_id, car, CAR_ACCEL, _fit_set.phase1_out[ACCEL]);

			move_cars(ids->track_id, T_STEP);

			if (car_crashed(ids->track_id, car)) break;
		}

		fit += get_track_completion(ids->track_id, car);
	}

	return fit / _fit_set.id_len;
}

double phase2_fit(int size, double *weights) {
	double fit = 0, single_dist, mult_dist;
	int sing_car, mult_car;
	struct track_ids *ids;

	for (int j = 0; j < _fit_set.id_len; j++) {
		ids = _fit_set.ids + j;
		for (int k = 0; k < 2; k++) {
			clear_cars(ids->track_id);
			if (k == 0) {
				sing_car = init_car(ids->track_id, ids->car_unit);
				mult_car = init_car(ids->track_id, ids->car_unit);
			} else {
				mult_car = init_car(ids->track_id, ids->car_unit);
				sing_car = init_car(ids->track_id, ids->car_unit);
			}

			for (int i = 0; i < _fit_set.n_steps; i++) {
				_fit_set.phase1_in[0] = get_car_velocity(ids->track_id, sing_car);
				_fit_set.phase1_in[1] = next_chckpt_ang(ids->track_id, sing_car);
				for (int i = 0; i < _fit_set.n_wall_angles; i++)
					_fit_set.phase1_in[i + 2] = get_wall_dist(ids->track_id, sing_car, _fit_set.wall_angles[i]);

				eval_neur_net(_fit_set.phase1_net, _fit_set.phase1_weights, _fit_set.phase1_in, _fit_set.phase1_out);
				set_car_input(ids->track_id, sing_car, CAR_STEER, _fit_set.phase1_out[STEER]);
				set_car_input(ids->track_id, sing_car, CAR_ACCEL, _fit_set.phase1_out[ACCEL]);

				_fit_set.phase2_in[0] = get_car_velocity(ids->track_id, mult_car);
				_fit_set.phase2_in[1] = next_chckpt_ang(ids->track_id, mult_car);
				for (int i = 0; i < _fit_set.n_wall_angles; i++) {
					_fit_set.phase2_in[i + 2] = get_wall_dist(ids->track_id, mult_car, _fit_set.wall_angles[i]);
				}
				for (int i = 0; i < _fit_set.n_car_angles; i++) {
					_fit_set.phase2_in[i + 2 + _fit_set.n_wall_angles] = get_wall_dist(ids->track_id, mult_car, _fit_set.wall_angles[i]);
				}

				eval_neur_net(_fit_set.phase2_net, weights, _fit_set.phase2_in, _fit_set.phase2_out);
				set_car_input(ids->track_id, mult_car, CAR_STEER, _fit_set.phase2_out[STEER]);
				set_car_input(ids->track_id, mult_car, CAR_ACCEL, _fit_set.phase2_out[ACCEL]);


				move_cars(ids->track_id, T_STEP);

				if (car_crashed(ids->track_id, mult_car)) break;
			}

			single_dist = get_track_completion(ids->track_id, sing_car);
			mult_dist = get_track_completion(ids->track_id, mult_car);
			fit += _fit_set.alpha * mult_dist - (1 - _fit_set.alpha) * (single_dist - mult_dist);
		}
	}

	return fit / _fit_set.id_len / 2;
}

double phase3_fit(int size, double *weights) {
	double fit = 0, dist1, dist2;
	int car1, car2;
	struct track_ids *ids;

	for (int j = 0; j < _fit_set.id_len; j++) {
		ids = _fit_set.ids + j;
		clear_cars(ids->track_id);
		car1 = init_car(ids->track_id, ids->car_unit);
		car2 = init_car(ids->track_id, ids->car_unit);

		for (int i = 0; i < _fit_set.n_steps; i++) {
			_fit_set.phase2_in[0] = get_car_velocity(ids->track_id, car1);
			_fit_set.phase2_in[1] = next_chckpt_ang(ids->track_id, car1);
			for (int i = 0; i < _fit_set.n_wall_angles; i++)
				_fit_set.phase2_in[i + 2] = get_wall_dist(ids->track_id, car1, _fit_set.wall_angles[i]);
			for (int i = 0; i < _fit_set.n_car_angles; i++) {
				_fit_set.phase2_in[i + 2 + _fit_set.n_wall_angles] = get_wall_dist(ids->track_id, car1, _fit_set.car_angles[i]);
			}

			eval_neur_net(_fit_set.phase2_net, weights, _fit_set.phase2_in, _fit_set.phase2_out);
			set_car_input(ids->track_id, car1, CAR_STEER, _fit_set.phase2_out[STEER]);
			set_car_input(ids->track_id, car1, CAR_ACCEL, _fit_set.phase2_out[ACCEL]);

			_fit_set.phase2_in[0] = get_car_velocity(ids->track_id, car2);
			_fit_set.phase2_in[1] = next_chckpt_ang(ids->track_id, car2);
			for (int i = 0; i < _fit_set.n_wall_angles; i++) {
				_fit_set.phase2_in[i + 2] = get_wall_dist(ids->track_id, car2, _fit_set.wall_angles[i]);
			}
			for (int i = 0; i < _fit_set.n_car_angles; i++) {
				_fit_set.phase2_in[i + 2 + _fit_set.n_wall_angles] = get_wall_dist(ids->track_id, car2, _fit_set.car_angles[i]);
			}

			eval_neur_net(_fit_set.phase2_net, weights, _fit_set.phase2_in, _fit_set.phase2_out);
			set_car_input(ids->track_id, car2, CAR_STEER, _fit_set.phase2_out[STEER]);
			set_car_input(ids->track_id, car2, CAR_ACCEL, _fit_set.phase2_out[ACCEL]);

			move_cars(ids->track_id, T_STEP);

			if (all_cars_crashed(ids->track_id)) break;
		}

		dist1 = get_track_completion(ids->track_id, car1);
		dist2 = get_track_completion(ids->track_id, car2);
		if (dist1 < dist2) {
			dist1 = dist1 + dist2;
			dist2 = dist1 - dist2;
			dist1 = dist1 - dist2;
		}
		fit += _fit_set.beta * dist1 - (1 - _fit_set.beta) * dist2;
	}

	return fit / _fit_set.id_len;
}

double test_fit(int size, double *weights) {
	double fit = 0, dist1, dist2;
	int car1, car2;
	struct track_ids *ids;

	for (int j = 0; j < _fit_test.id_len; j++) {
		ids = _fit_test.ids + j;
		clear_cars(ids->track_id);
		car1 = init_car(ids->track_id, ids->car_unit);
		car2 = init_car(ids->track_id, ids->car_unit);

		for (int i = 0; i < _fit_test.n_steps; i++) {
			_fit_test.in[0] = get_car_velocity(ids->track_id, car1);
			_fit_test.in[1] = next_chckpt_ang(ids->track_id, car1);
			for (int i = 0; i < _fit_test.n_wall_angles; i++)
				_fit_test.in[i + 2] = get_wall_dist(ids->track_id, car1, _fit_test.wall_angles[i]);
			for (int i = 0; i < _fit_test.n_car_angles; i++) {
				_fit_test.in[i + 2 + _fit_test.n_wall_angles] = get_wall_dist(ids->track_id, car1, _fit_test.car_angles[i]);
			}

			eval_neur_net(_fit_test.net, weights, _fit_test.in, _fit_test.out);
			set_car_input(ids->track_id, car1, CAR_STEER, _fit_test.out[STEER]);
			set_car_input(ids->track_id, car1, CAR_ACCEL, _fit_test.out[ACCEL]);

			_fit_test.in[0] = get_car_velocity(ids->track_id, car2);
			_fit_test.in[1] = next_chckpt_ang(ids->track_id, car2);
			for (int i = 0; i < _fit_test.n_wall_angles; i++)
				_fit_test.in[i + 2] = get_wall_dist(ids->track_id, car2, _fit_test.wall_angles[i]);
			for (int i = 0; i < _fit_test.n_car_angles; i++) {
				_fit_test.in[i + 2 + _fit_test.n_wall_angles] = get_wall_dist(ids->track_id, car2, _fit_test.car_angles[i]);
			}

			eval_neur_net(_fit_test.net, weights, _fit_test.in, _fit_test.out);
			set_car_input(ids->track_id, car2, CAR_STEER, _fit_test.out[STEER]);
			set_car_input(ids->track_id, car2, CAR_ACCEL, _fit_test.out[ACCEL]);

			move_cars(ids->track_id, T_STEP);

			if (all_cars_crashed(ids->track_id)) break;
		}

		dist1 = get_track_completion(ids->track_id, car1);
		dist2 = get_track_completion(ids->track_id, car2);
		fit += (dist1 + dist2) / 2;
	}

	return fit / _fit_set.id_len;
}

void set_phase1_weights(double *weights) {
	_fit_set.phase1_weights = weights;
}

void release_tracks() {
	if (_fitness_tracks_set == 0) return;

	release_track_ids(_fit_set.id_len, _fit_set.ids);
	_fitness_tracks_set = 0;
}

void release_fitness() {
	if (_fitness_set == 0) return;

//	release_neur_net(_fit_set.phase1_net);
//	release_neur_net(_fit_set.phase2_net);

	free(_fit_set.phase1_in);
	free(_fit_set.phase1_out);

	free(_fit_set.phase2_in);
	free(_fit_set.phase2_out);

	free(_fit_set.wall_angles);
	free(_fit_set.car_angles);

	_fitness_set = 0;
}

int init_fitness_tracks(int track_n, struct track_ids *ids) {
	_fit_set.id_len = track_n;
	_fit_set.ids = ids;
	_fitness_tracks_set = 1;
	return 0;
}

int init_fitness(struct neural_net *phase1_net, struct neural_net *phase2_net, int n_wall_angles,
				int n_car_angles, double alpha, double beta, int n_steps) {
	if (_fitness_set)
		release_fitness();

	_fit_set.phase1_net = phase1_net;
	_fit_set.phase1_buf[IND_IN] = input_size(phase1_net);
	_fit_set.phase1_buf[IND_OUT] = output_size(phase1_net);
	if (_fit_set.phase1_buf[IND_IN] != n_wall_angles + 2) {
		fprintf(stderr, "Input layer is of size %d, but should be %d\n", _fit_set.phase1_buf[IND_IN], n_wall_angles + 2);
		return -1;
	}

	_fit_set.phase2_net = phase2_net;
	_fit_set.phase2_buf[IND_IN] = input_size(phase2_net);
	_fit_set.phase2_buf[IND_OUT] = output_size(phase2_net);
	if (_fit_set.phase2_buf[IND_IN] != n_wall_angles + n_car_angles + 2) {
		fprintf(stderr, "Input layer is of size %d, but should be %d\n", _fit_set.phase2_buf[IND_IN], n_wall_angles + n_car_angles + 2);
		return -1;
	}

	_fit_set.n_wall_angles = n_wall_angles;
	_fit_set.n_car_angles = n_car_angles;

	_fit_set.alpha = alpha;
	_fit_set.beta = beta;
	_fit_set.n_steps = n_steps;

	_fit_set.phase1_in = (double*) malloc(_fit_set.phase1_buf[IND_IN] * sizeof(double));
	_fit_set.phase1_out = (double*) malloc(_fit_set.phase1_buf[IND_OUT] * sizeof(double));
	_fit_set.phase2_in = (double*) malloc(_fit_set.phase2_buf[IND_IN] * sizeof(double));
	_fit_set.phase2_out = (double*) malloc(_fit_set.phase2_buf[IND_OUT] * sizeof(double));

	_fit_set.wall_angles = (double*) malloc(n_wall_angles * sizeof(double));
	for (int i = 0; i < n_wall_angles; i++)
		_fit_set.wall_angles[i] = i * 2 * M_PI / n_wall_angles;

	_fit_set.car_angles = (double*) malloc(n_car_angles * sizeof(double));
	for (int i = 0; i < n_car_angles; i++)
		_fit_set.car_angles[i] = i * 2 * M_PI / n_car_angles;

	_fitness_set = 1;
	return 0;
}

void release_test_tracks() {
	if (_fitness_test_tracks_set == 0) return;

	release_track_ids(_fit_test.id_len, _fit_test.ids);
	_fitness_test_tracks_set = 0;
}

void release_test_fitness() {
	if (_fitness_test_set == 0) return;

//	release_neur_net(_fit_test.net);

	free(_fit_test.in);
	free(_fit_test.out);

	free(_fit_test.wall_angles);
	free(_fit_test.car_angles);

	_fitness_test_set = 0;
}

int init_test_fitness_tracks(int track_n, struct track_ids *ids) {
	_fit_test.id_len = track_n;
	_fit_test.ids = ids;
	_fitness_test_tracks_set = 1;
	return 0;
}

int init_test_fitness(struct neural_net *net, int n_wall_angles, int n_car_angles, int n_steps) {
	if (_fitness_test_set)
		release_test_fitness();

	_fit_test.net = net;
	_fit_test.buf_len[IND_IN] = input_size(net);
	_fit_test.buf_len[IND_OUT] = output_size(net);
	if (_fit_test.buf_len[IND_IN] != n_wall_angles + n_car_angles + 2) {
		fprintf(stderr, "Input layer is of size %d, but should be %d\n", _fit_test.buf_len[IND_IN], n_wall_angles + n_car_angles + 2);
		return -1;
	}

	_fit_test.n_wall_angles = n_wall_angles;
	_fit_test.n_car_angles = n_car_angles;
	_fit_test.n_steps = n_steps;

	_fit_test.in = (double*) malloc(_fit_test.buf_len[IND_IN] * sizeof(double));
	_fit_test.out = (double*) malloc(_fit_test.buf_len[IND_OUT] * sizeof(double));

	_fit_test.wall_angles = (double*) malloc(n_wall_angles * sizeof(double));
	for (int i = 0; i < n_wall_angles; i++)
		_fit_test.wall_angles[i] = i * 2 * M_PI / n_wall_angles;

	_fit_test.car_angles = (double*) malloc(n_car_angles * sizeof(double));
	for (int i = 0; i < n_car_angles; i++)
		_fit_test.car_angles[i] = i * 2 * M_PI / n_car_angles;

	_fitness_test_set = 1;
	return 0;
}

int conf_init_fitness(struct configuration *config) {
	int track_n, n_wall_angles, n_car_angles, n_steps;
	double alpha, beta;
	struct track_ids *ids;
	struct neural_net *phase1_net, *phase2_net;
	struct configuration *sub_config;

	if (conf_load_tracks(&ids, &track_n, config) < 0) {
		fprintf(stderr, "cannot load tracks\n");
		return -1;
	}

	sub_config = get_sub_config(config, _FIT_PHASE1);
	if ((phase1_net = conf_load_neur_net(sub_config)) == NULL) return -1;
	sub_config = get_sub_config(config, _FIT_PHASE2);
	if ((phase2_net = conf_load_neur_net(sub_config)) == NULL) return -1;

	if (get_config_integer(config, _FIT_WALL_SENS, &n_wall_angles) == CONF_ERR) return -1;
	if (get_config_integer(config, _FIT_CAR_SENS, &n_car_angles) == CONF_ERR) return -1;
	if (get_config_integer(config, _FIT_STEPS, &n_steps) == CONF_ERR) return -1;

	if (get_config_double(config, _FIT_ALPHA, &alpha) == CONF_ERR) return -1;
	if (get_config_double(config, _FIT_BETA, &beta) == CONF_ERR) return -1;

	init_fitness_tracks(track_n, ids);
	return init_fitness(phase1_net, phase2_net, n_wall_angles, n_car_angles, alpha, beta, n_steps);
}
