#ifndef _H_FITNESS
#define _H_FITNESS

#include <model.h>

#define _FIT_PHASE1 "phase1"
#define _FIT_PHASE2 "phase2"
#define _FIT_WALL_SENS "number.of.wall.sensors"
#define _FIT_CAR_SENS "number.of.car.sensors"
#define _FIT_STEPS "number.of.steps"
#define _FIT_ALPHA "fitness.alpha"
#define _FIT_BETA "fitness.beta"

double phase1_fit(int size, double *weights);

double phase2_fit(int size, double *weights);

double phase3_fit(int size, double *weights);

double test_fit(int size, double *weights);

void set_phase1_weights(double *weigths);

int init_fitness_tracks(int track_n, struct track_ids *ids);

int init_fitness(struct neural_net *phase1_net, struct neural_net *phase2_net, int n_wall_angles,
				int n_car_angles, double alpha, double beta, int n_steps);

void release_tracks();

void release_fitness();

int init_test_fitness_tracks(int track_n, struct track_ids *ids);

int init_test_fitness(struct neural_net *net, int n_wall_angles, int n_car_angles, int n_steps);

void release_test_tracks();

void release_test_fitness();

int conf_init_fitness(struct configuration *config);

#endif
