#include <math.h>
#include <GL/glut.h>
#include <stdio.h>
#include <model.h>
#include <neur_net.h>
#include <config.h>
#include <fitness.h>

#define ACCEL 1
#define STEER 0
#define X_CORD 0
#define Y_CORD 1
#define UNIT 50.0
#define ERROR 1
#define CHECKPT_S 3

#define T_STEP 0.02

int red_car, blue_car, track_id, ids_len, train_ids_len, test_ids_len, cur_track, n_wall_angles, n_car_angles, w_size, using_train = 1, enter_pressed = 0;
double *in, out[2], *wall_angles, *car_angles, *weights;
struct track_ids *train_ids, *test_ids, *ids;
struct neural_net *net;

void draw_line(double x0, double y0, double x1, double y1) {
	glVertex2d(x0, y0);
	glVertex2d(x1, y1);
}

void draw_quad_point(double x, double y) {
	glVertex2d(x, y);
}

void draw_track_fun() {
	glBegin(GL_LINES);
	draw_track(track_id, &draw_line);
	glEnd();
}

void draw_checkpoint(double x, double y) {
	glVertex2d(x - CHECKPT_S, y - CHECKPT_S);
	glVertex2d(x - CHECKPT_S, y + CHECKPT_S);
	glVertex2d(x + CHECKPT_S, y + CHECKPT_S);
	glVertex2d(x + CHECKPT_S, y - CHECKPT_S);
}

void draw_checkpoints() {
	int chckpt_size = get_chckpt_size(track_id);
	double x[2];

	glBegin(GL_QUADS);
	for (int i = 0; i < chckpt_size; i++) {
		get_chckpt(track_id, i, x);
		draw_checkpoint(x[0], x[1]);
	}
	glEnd();
}

void draw_closest_chckpt(int id) {
	double pos[2];
	get_car_position(track_id, id, pos);
	int index = get_closest_checkpt(track_id, pos);
	double x[2];

	glBegin(GL_QUADS);
	get_chckpt(track_id, index, x);
	draw_checkpoint(x[0], x[1]);
	glEnd();
}

void draw_closest_point(int id) {
	double pos[2];
	double x[2];
	get_car_position(track_id, id, pos);

	get_closest_point(track_id, pos, x);
	glBegin(GL_QUADS);
	draw_checkpoint(x[0], x[1]);
	glEnd();
}

void connect_checkpoints() {
	int chckpt_size = get_chckpt_size(track_id);
	double x[2];

	glBegin(GL_LINE_LOOP);
	for (int i = 0; i < chckpt_size; i++) {
		get_chckpt(track_id, i, x);
		glVertex2d(x[0], x[1]);
	}
	glEnd();
}

void draw_square() {
	glBegin(GL_TRIANGLE_STRIP);
	glVertex2d(-10, 40);
	glVertex2d(10, 40);
	glVertex2d(-10, 0);
	glVertex2d(10, 0);
	glEnd();
}

void draw_car_fun(int id) {
	glBegin(GL_QUADS);
	draw_car(track_id, id, &draw_quad_point);
	glEnd();
}

void render_scene() {
	glPointSize(1);
	glColor3d(0.0, 0.0, 0.0);
	draw_track(track_id, &draw_track_fun);

	glColor3d(1.0, 0.7, 0.0);
	draw_checkpoints();
	connect_checkpoints();

	glColor3d(0.8, 0, 0.3);
	draw_closest_chckpt(red_car);
	draw_closest_point(red_car);
	draw_car_fun(red_car);

	glColor3d(0.0, 0, 0.6);;
	draw_closest_chckpt(blue_car);
	draw_closest_point(blue_car);
	draw_car_fun(blue_car);
}

void display() {
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();
	render_scene();
	glutSwapBuffers();
}

void reshape(int width, int height) {
	glDisable(GL_DEPTH_TEST);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, width - 1, 0, height - 1, 0, 1);
	glViewport(0, 0,(GLsizei) width, (GLsizei) height);
	glMatrixMode(GL_MODELVIEW);
}

void animate(int value) {
	in[0] = get_car_velocity(track_id, blue_car);
	in[1] = next_chckpt_ang(track_id, blue_car);
	for (int i = 0; i < n_wall_angles; i++)
		in[i + 2] = get_wall_dist(track_id, blue_car, wall_angles[i]);
	for (int i = 0; i < n_car_angles; i++) {
		in[i + 2 + n_wall_angles] = get_wall_dist(track_id, blue_car, car_angles[i]);
	}

	eval_neur_net(net, weights, in, out);
	set_car_input(track_id, blue_car, CAR_STEER, out[STEER]);
	set_car_input(track_id, blue_car, CAR_ACCEL, out[ACCEL]);

	in[0] = get_car_velocity(track_id, red_car);
	in[1] = next_chckpt_ang(track_id, red_car);
	for (int i = 0; i < n_wall_angles; i++)
		in[i + 2] = get_wall_dist(track_id, red_car, wall_angles[i]);
	for (int i = 0; i < n_car_angles; i++) {
		in[i + 2 + n_wall_angles] = get_wall_dist(track_id, red_car, car_angles[i]);
	}

	eval_neur_net(net, weights, in, out);
	set_car_input(track_id, red_car, CAR_STEER, out[STEER]);
	set_car_input(track_id, red_car, CAR_ACCEL, out[ACCEL]);

	move_cars(track_id, T_STEP);

	if (all_cars_crashed(track_id) || enter_pressed) {
		if (enter_pressed)
			enter_pressed--;
		cur_track++;
		if (using_train && cur_track == train_ids_len) {
			cur_track = 0;
			if (test_ids_len != 0) {
				ids = test_ids;
				using_train = 0;
			}
		} else if (!using_train && cur_track == test_ids_len) {
			cur_track = 0;
			ids = train_ids;
			using_train = 1;
		}
		track_id = ids[cur_track].track_id;
		clear_cars(track_id);
		ids[cur_track].car_ids[0] = init_car(ids[cur_track].track_id, ids[cur_track].car_unit);
		ids[cur_track].car_ids[1] = init_car(ids[cur_track].track_id, ids[cur_track].car_unit);
		blue_car = ids[cur_track].car_ids[0];
		red_car = ids[cur_track].car_ids[1];
	}

	glutPostRedisplay();
	glutTimerFunc(20, animate, 0);
}

int init(char *config_filename) {
	struct configuration *test_config, *config = config_file(config_filename);
	test_config = get_sub_config(config, "test");

	if (conf_load_tracks(&train_ids, &train_ids_len, config) == -1) return ERROR;
	if (conf_load_tracks(&test_ids, &test_ids_len, test_config) == -1) return ERROR;
	if ((net = conf_load_neur_net(config)) == NULL) return ERROR;

	if (get_config_integer(config, _FIT_WALL_SENS, &n_wall_angles) == CONF_ERR) return ERROR;
	if (get_config_integer(config, _FIT_CAR_SENS, &n_car_angles) == CONF_ERR) return ERROR;
	if (get_config_integer(config, "result.size", &w_size) == CONF_ERR) return ERROR;
	if (get_config_double_array(config, "result.weights", w_size, &weights) == CONF_ERR) return ERROR;

	in = (double*) malloc(input_size(net) * sizeof(double));

	wall_angles = (double*) malloc(n_wall_angles * sizeof(double));
	for (int i = 0; i < n_wall_angles; i++)
		wall_angles[i] = i * 2 * M_PI / n_wall_angles;

	car_angles = (double*) malloc(n_car_angles * sizeof(double));
	for (int i = 0; i < n_car_angles; i++)
		car_angles[i] = i * 2 * M_PI / n_car_angles;

	return 0;
}

void get_bound_box(double *glob_min, double *glob_max) {
	double min[2], max[2];

	get_track_bound_box(train_ids[0].track_id, glob_min, glob_max);

	for (int i = 1; i < train_ids_len; i++) {
		get_track_bound_box(train_ids[i].track_id, min, max);
		for (int j = 0; j < 2; j++) {
			if (min[j] < glob_min[j]) glob_min[j] = min[j];
			if (max[j] > glob_max[j]) glob_max[j] = max[j];
		}
	}
	for (int i = 0; i < test_ids_len; i++) {
		get_track_bound_box(test_ids[i].track_id, min, max);
		for (int j = 0; j < 2; j++) {
			if (min[j] < glob_min[j]) glob_min[j] = min[j];
			if (max[j] > glob_max[j]) glob_max[j] = max[j];
		}
	}
}

void enter_pressed_fun(unsigned char key, int x, int y) {
	//printf("got key %d ", key);
	//fflush(stdout);
	if (key == 13) {
		enter_pressed++;
	}
}

int main(int argc, char **argv) {
	double min[2], max[2];

	if (argc < 2) {
		fprintf(stderr, "Usage: %s result_file\n", argv[0]);
		return 1;
	}

	if (init(argv[1]) == ERROR) {
		fprintf(stderr, "Cannot initialise stuff\n");
		return 1;
	}
	get_bound_box(min, max);

	ids = train_ids;
	track_id = ids[0].track_id;
	cur_track = 0;
	ids[0].car_ids[0] = init_car(ids[0].track_id, ids[0].car_unit);
	ids[0].car_ids[1] = init_car(ids[0].track_id, ids[0].car_unit);
	blue_car = ids[0].car_ids[0];
	red_car = ids[0].car_ids[1];

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE);
	glutInitWindowSize(max[0] + min[0], max[1] + min[1]);
	glutCreateWindow("Fitness demo");

	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(enter_pressed_fun);
	glutTimerFunc(20, animate, 0);
	glutMainLoop();

	return 0;
}

