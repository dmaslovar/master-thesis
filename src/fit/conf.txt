# phase 1 neural network
phase1.architecture.length = 4
phase1.architecture = 10 15 8 2
phase1.architecture.hidden.activation = sigmoid
phase1.architecture.output.activation = tanh

# phase 1 evolutionary algorithm
evol.phase1.k.tournament = 3
evol.phase1.number.of.generations = 50
evol.phase1.mutation.mean = 0
evol.phase1.mutation.deviation = 0.1
evol.phase1.mutation.probability = 0.1
evol.phase1.population.generation.method = random.population
evol.phase1.population.size = 50
evol.phase1.chromosome.length = 311
evol.phase1.chromosome.mean = 0
evol.phase1.chromosome.deviation = 1

# phase 2 neural network
phase2.architecture.length = 4
phase2.architecture = 18 20 10 2
phase2.architecture.hidden.activation = sigmoid
phase2.architecture.output.activation = tanh

# phase 2 evolutionary algorithm
evol.phase2.k.tournament = 3
evol.phase2.number.of.generations = 50
evol.phase2.mutation.mean = 0
evol.phase2.mutation.deviation = 0.1
evol.phase2.mutation.probability = 0.1
evol.phase2.population.size = 50
evol.phase2.old.architecture.length = 4
evol.phase2.old.architecture = 10 15 8 2
evol.phase2.new.architecture.length = 4
evol.phase2.new.architecture = 18 20 10 2
evol.phase2.new.chromosome.length = 612
evol.phase2.chromosome.mean = 0
evol.phase2.chromosome.deviation = 1

# phase 3 evolutionary algorithm
evol.phase3.k.tournament = 3
evol.phase3.number.of.generations = 50
evol.phase3.mutation.mean = 0
evol.phase3.mutation.deviation = 0.1
evol.phase3.mutation.probability = 0.1
evol.phase3.population.size = 50
evol.phase3.chromosome.mean = 0
evol.phase3.chromosome.deviation = 1

test.architecture.length = 4
test.architecture = 18 20 10 2
test.architecture.hidden.activation = sigmoid
test.architecture.output.activation = tanh

# sensors
number.of.wall.sensors = 8
number.of.car.sensors = 8
test.number.of.wall.sensors = 8
test.number.of.car.sensors = 8

# number of evaluation steps
number.of.steps = 100
test.number.of.steps = 100

# hyperparameters
fitness.alpha = 0.7
fitness.beta = 0.7

# tracks
number.of.tracks = 3
track.files = ../tracks/left1.txt ../tracks/left2.txt ../tracks/left4.txt
number.of.cars = 2
car.unit = 50
test.number.of.tracks = 1
test.track.files = ../tracks/left3.txt
test.number.of.cars = 2
test.car.unit = 50
