#include <stdio.h>
#include <float.h>
#include <config.h>
#include <neur_net.h>
#include <evol_alg.h>
#include <model.h>
#include <fitness.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#define CONFIG "conf.txt"
#define RESULT "result.txt"

int phase = 1;

void log_phase(int gen, double best) {
	if (gen % 500 != 0) return;
	printf("phase: %d gen: %d best: %lf\r", phase, gen, best);
	fflush(stdout);
}

double *preserve_best(struct population *pop, int *w_size) {
	int best_i;
	double best = -DBL_MAX, *w;

	for (int i = 0; i < pop->size; i++)
		if (pop->pop[i].fit > best) {
			best = pop->pop[i].fit;
			best_i = i;
		}

	w = (double*) malloc(pop->pop[best_i].size * sizeof(double));
	memcpy(w, pop->pop[best_i].weights, pop->pop[best_i].size * sizeof(double));
	*w_size = pop->pop[best_i].size;
	return w;
}

void write_results(FILE *f, int w_size, double *w, double fitness, struct configuration *config) {
	char *buf;

	fprintf(f, "# results\nresult.size = %d\nresult.weights =", w_size);
	for (int i = 0; i < w_size; i++)
		fprintf(f, " %lf", w[i]);
	fprintf(f, "\nwinner.fitness = %lf\n", fitness);

	fprintf(f, "\n\n# neural network\n");
	get_config_value(config, "phase2.architecture.length", &buf);
	fprintf(f, "architecture.length = %s\n", buf);
	get_config_value(config, "phase2.architecture", &buf);
	fprintf(f, "architecture = %s\n", buf);
	get_config_value(config, "phase2.architecture.hidden.activation", &buf);
	fprintf(f, "architecture.hidden.activation = %s\n", buf);
	get_config_value(config, "phase2.architecture.output.activation", &buf);
	fprintf(f, "architecture.output.activation = %s\n", buf);

	fprintf(f, "\n# input settings\n");
	get_config_value(config, "number.of.wall.sensors", &buf);
	fprintf(f, "number.of.wall.sensors = %s\n", buf);
	get_config_value(config, "number.of.car.sensors", &buf);
	fprintf(f, "number.of.car.sensors = %s\n", buf);

	fprintf(f, "\n# track settings\n");
	get_config_value(config, "number.of.tracks", &buf);
	fprintf(f, "number.of.tracks = %s\n", buf);
	get_config_value(config, "track.files", &buf);
	fprintf(f, "track.files = %s\n", buf);
	get_config_value(config, "number.of.cars", &buf);
	fprintf(f, "number.of.cars = %s\n", buf);
	get_config_value(config, "car.unit", &buf);
	fprintf(f, "car.unit = %s\n", buf);
	get_config_value(config, "test.number.of.tracks", &buf);
	fprintf(f, "test.number.of.tracks = %s\n", buf);
	get_config_value(config, "test.track.files", &buf);
	fprintf(f, "test.track.files = %s\n", buf);
	get_config_value(config, "test.number.of.cars", &buf);
	fprintf(f, "test.number.of.cars = %s\n", buf);
	get_config_value(config, "test.car.unit", &buf);
	fprintf(f, "test.car.unit = %s\n", buf);
}

int init_test_config(struct configuration *config) {
	int test_ids_len;
	struct track_ids *test_ids;
	struct neural_net *test_net;
	int test_steps, test_walls, test_cars;

	if (conf_load_tracks(&test_ids, &test_ids_len, config) < 0) {
		fprintf(stderr, "cannot init test tracks\n");
		return 1;
	}
	if (get_config_integer(config, "number.of.wall.sensors", &test_walls) == CONF_ERR) return 1;
	if (get_config_integer(config, "number.of.car.sensors", &test_cars) == CONF_ERR) return 1;
	if (get_config_integer(config, "number.of.steps", &test_steps) == CONF_ERR) return 1;
	if ((test_net = conf_load_neur_net(config)) == NULL) return 1;

	init_test_fitness_tracks(test_ids_len, test_ids);
	if (init_test_fitness(test_net, test_walls, test_cars, test_steps) == -1) return 1;
	return 0;
}

int main(int argc, char **argv) {
	double *p1_best, *p2_best, *p3_best;
	int p1_best_size, p2_best_size, p3_best_size;
	struct evolution *evol1, *evol2, *evol3;
	struct configuration *sub_conf1, *sub_conf2, *sub_conf3, *config, *test_config;
	struct population *pop;
	double best_fit;
	FILE *f;

	if (argc != 3) {
		fprintf(stderr, "Usage: %s config_file result_file\n", argv[0]);
		return 1;
	}

	if ((config = config_file(argv[1])) == NULL) {
		fprintf(stderr, "config NULL\n");
		return 1;
	}
	if (conf_init_fitness(config) < 0) {
		fprintf(stderr, "cannot init fitness\n");
		return 1;
	}

	test_config = get_sub_config(config, "test");
	if (init_test_config(test_config) == 1) {
		fprintf(stderr, "Cannot init test fitness\n");
		return 1;
	}

	sub_conf1 = get_sub_config(config, "evol.phase1");
	if ((evol1 = conf_new_evolution(sub_conf1)) == NULL) {
		fprintf(stderr, "cannot init evol1\n");
		return 1;
	}
	evol1->fit = phase1_fit;

	sub_conf2 = get_sub_config(config, "evol.phase2");
	if ((evol2 = conf_new_evolution_skip_pop(sub_conf2)) == NULL) {
		fprintf(stderr, "cannot init evol2\n");
		return 1;
	}
	evol2->fit = phase2_fit;

	sub_conf3 = get_sub_config(config, "evol.phase3");
	if ((evol3 = conf_new_evolution_skip_pop(sub_conf3)) == NULL) {
		fprintf(stderr, "cannot init evol3\n");
		return 1;
	}
	evol3->fit = phase3_fit;

	printf("Loaded configs done\n");

	learn_evol(evol1, log_phase);
	p1_best = preserve_best(evol1->pop, &p1_best_size);
	set_phase1_weights(p1_best);
	release_evolution(evol1);
	printf("\n");
	phase++;

	if ((pop = conf_new_arch_pop_from_pretrained_w(sub_conf2, p1_best_size, p1_best)) == NULL) {
		fprintf(stderr, "cannot init evol2 pop\n");
		return 1;
	}
	evol2->pop = pop;
	learn_evol(evol2, log_phase);
	p2_best = preserve_best(evol2->pop, &p2_best_size);
	release_evolution(evol2);
	printf("\n");
	phase++;

	if ((pop = conf_pop_from_pretrained_w(sub_conf3, p2_best_size, p2_best)) == NULL) {
		fprintf(stderr, "cannot init evol3 pop\n");
		return 1;
	}
	evol3->pop = pop;
	learn_evol(evol3, log_phase);
	p3_best = preserve_best(evol3->pop, &p3_best_size);
	release_evolution(evol3);
	printf("\n");

	best_fit = test_fit(p3_best_size, p3_best);

	if ((f = fopen(argv[2], "w")) == NULL) {
		fprintf(stderr, "Cannot open result file: %s\n", strerror(errno));
		return 1;
	}

	write_results(f, p3_best_size, p3_best, best_fit, config);

	fclose(f);
}
