#! /bin/bash
HEADERS=(../util/gauss/gauss.h ../util/unit_test/test.h ../util/conf/config.h ../evol/evol_alg.h ../neur/neur_net.h ../model/model.h ../fit/fitness.h)
for file in ${HEADERS[@]}; do
	ln -s ${file} .
done;
