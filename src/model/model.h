#ifndef _CAR_TRACK_MODEL_H
#define _CAR_TRACK_MODEL_H

#include <config.h>

#define CAR_INS 2
#define CAR_STEER 0
#define CAR_ACCEL 1

#define _MODEL_TRACK_NUM "number.of.tracks"
#define _MODEL_TRACK_FILE "track.files"
#define _MODEL_CAR_NUM "number.of.cars"
#define _MODEL_CAR_UNIT "car.unit"

struct track_ids {
	int track_id;
	int car_n;
	double car_unit;
	int *car_ids;
};

int init_car(int track_id, double unit);

void set_car_input(int track_id, int car_id, int in_id, double val);

void move_cars(int track_id, double dt);

int all_cars_crashed(int track_id);

int car_crashed(int track_id, int id);

double get_car_angle(int track_id, int car_id);

void get_car_position(int track_id, int car_id, double *p);

double get_car_velocity(int track_id, int car_id);

void draw_car(int track_id, int id, void (*draw_quad_point)(double, double));

int get_closest_checkpt(int track_id, double *p);

void get_closest_point(int track_id, double *p, double *x);

double get_track_completion(int track_id, int car_id);

double next_chckpt_ang(int track_id, int car_id);

void get_track_bound_box(int track_id, double *min, double *max);

int load_track(char *filename);

void draw_track(int track_id, void (*draw_line)(double, double, double, double));

int get_chckpt_size(int track_id);

void get_chckpt(int track_id, int index, double *x);

double get_wall_dist(int track_id, int car_id, double radians);

double get_car_dist(int track_id, int car_id, double radians);

void clear_cars(int track_id);

int conf_load_tracks(struct track_ids **ids, int *n, struct configuration *config);

void release_track_ids(int n, struct track_ids *ids);

#endif
