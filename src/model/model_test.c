#include <math.h>
#include <GL/glut.h>
#include <stdio.h>
#include "model.h"

#define ACCEL 1.0
#define STEER 1.0
#define X_CORD 0
#define Y_CORD 1
#define UNIT 50.0
#define ERROR 1
#define TRACK_F "../test/car_neur_ev/track_long.txt"
#define CHECKPT_S 3

struct command_struc {
	int up;
	int down;
	int left;
	int right;
};

int red_car, blue_car, crashed[2] = {0}, track_id;
struct command_struc red_comms, blue_comms;

void draw_line(double x0, double y0, double x1, double y1) {
	glVertex2d(x0, y0);
	glVertex2d(x1, y1);
}

void draw_quad_point(double x, double y) {
	glVertex2d(x, y);
}

void draw_track_fun() {
	glBegin(GL_LINES);
	draw_track(track_id, &draw_line);
	glEnd();
}

void draw_checkpoint(double x, double y) {
	glVertex2d(x - CHECKPT_S, y - CHECKPT_S);
	glVertex2d(x - CHECKPT_S, y + CHECKPT_S);
	glVertex2d(x + CHECKPT_S, y + CHECKPT_S);
	glVertex2d(x + CHECKPT_S, y - CHECKPT_S);
}

void draw_checkpoints() {
	int chckpt_size = get_chckpt_size(track_id);
	double x[2];

	glBegin(GL_QUADS);
	for (int i = 0; i < chckpt_size; i++) {
		get_chckpt(track_id, i, x);
		draw_checkpoint(x[0], x[1]);
	}
	glEnd();
}

void draw_closest_chckpt(int id) {
	double pos[2];
	get_car_position(track_id, id, pos);
	int index = get_closest_checkpt(track_id, pos);
	double x[2];

	glBegin(GL_QUADS);
	get_chckpt(track_id, index, x);
	draw_checkpoint(x[0], x[1]);
	glEnd();
}

void draw_closest_point(int id) {
	double pos[2];
	double x[2];
	get_car_position(track_id, id, pos);

	get_closest_point(track_id, pos, x);
	glBegin(GL_QUADS);
	draw_checkpoint(x[0], x[1]);
	glEnd();
}

void connect_checkpoints() {
	int chckpt_size = get_chckpt_size(track_id);
	double x[2];

	glBegin(GL_LINE_LOOP);
	for (int i = 0; i < chckpt_size; i++) {
		get_chckpt(track_id, i, x);
		glVertex2d(x[0], x[1]);
	}
	glEnd();
}

void draw_square() {
	glBegin(GL_TRIANGLE_STRIP);
	glVertex2d(-10, 40);
	glVertex2d(10, 40);
	glVertex2d(-10, 0);
	glVertex2d(10, 0);
	glEnd();
}

void draw_car_fun(int id) {
	glBegin(GL_QUADS);
	draw_car(track_id, id, &draw_quad_point);
	glEnd();
}

void render_scene() {
	glPointSize(1);
	glColor3d(0.0, 0.0, 0.0);
	draw_track(track_id, &draw_track_fun);

	glColor3d(1.0, 0.7, 0.0);
	draw_checkpoints();
	connect_checkpoints();

/*	glColor3d(0.8, 0, 0.3);
	draw_closest_chckpt(red_car);
	draw_closest_point(red_car);
	draw_car_fun(red_car);
*/
	glColor3d(0.0, 0, 0.6);;
	draw_closest_chckpt(blue_car);
	draw_closest_point(blue_car);
	draw_car_fun(blue_car);
}

void display() {
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();
	render_scene();
	glutSwapBuffers();
}

void reshape(int width, int height) {
	glDisable(GL_DEPTH_TEST);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, width - 1, 0, height - 1, 0, 1);
	glViewport(0, 0,(GLsizei) width, (GLsizei) height);
	glMatrixMode(GL_MODELVIEW);
}

void print_completion() {
	printf("red: %.3lf, blue: %.3lf, blue's right: %.3lf\r",
			get_track_completion(track_id, red_car),
			get_track_completion(track_id, blue_car),
			get_car_dist(track_id, blue_car, -M_PI/2));
}

void animate(int value) {
//	set_car_input(track_id, red_car, CAR_STEER, STEER * (red_comms.left - red_comms.right));
//	set_car_input(track_id, red_car, CAR_ACCEL, ACCEL * (red_comms.up - red_comms.down));
	set_car_input(track_id, blue_car, CAR_STEER, STEER * (blue_comms.left - blue_comms.right));
	set_car_input(track_id, blue_car, CAR_ACCEL, ACCEL * (blue_comms.up - blue_comms.down));

	move_cars(track_id, 0.02);
//	print_completion();

	glutPostRedisplay();
	glutTimerFunc(20, animate, 0);
}

void keyPressed(unsigned char key, int x, int y) {
	switch (key) {
	case 'w':
		red_comms.up = 1;
		break;
	case 's':
		red_comms.down = 1;
		break;
	case 'd':
		red_comms.right = 1;
		break;
	case 'a':
		red_comms.left = 1;
		break;
	}
}

void keyReleased(unsigned char key, int x, int y) {
	switch (key) {
	case 'w':
		red_comms.up = 0;
		break;
	case 's':
		red_comms.down = 0;
		break;
	case 'd':
		red_comms.right = 0;
		break;
	case 'a':
		red_comms.left = 0;
		break;
	}
}

void arrowPressed(int key, int x, int y) {
	switch (key) {
	case GLUT_KEY_UP:
		blue_comms.up = 1;
		break;
	case GLUT_KEY_DOWN:
		blue_comms.down = 1;
		break;
	case GLUT_KEY_RIGHT:
		blue_comms.right = 1;
		break;
	case GLUT_KEY_LEFT:
		blue_comms.left = 1;
		break;
	}
}

void arrowReleased(int key, int x, int y) {
	switch (key) {
	case GLUT_KEY_UP:
		blue_comms.up = 0;
		break;
	case GLUT_KEY_DOWN:
		blue_comms.down = 0;
		break;
	case GLUT_KEY_RIGHT:
		blue_comms.right = 0;
		break;
	case GLUT_KEY_LEFT:
		blue_comms.left = 0;
		break;
	}
}

void init() {
	red_comms.up = 0;
	red_comms.down = 0;
	red_comms.left = 0;
	red_comms.right = 0;

	blue_comms.up = 0;
	blue_comms.down = 0;
	blue_comms.left = 0;
	blue_comms.right = 0;

//	red_car = init_car(track_id, UNIT);

	blue_car = init_car(track_id, UNIT);
}

int init_track() {
	if ((track_id = load_track(TRACK_F)) == -1) return ERROR;
	return 0;
}

int main(int argc, char **argv) {
	double min[2], max[2];
	if (init_track(argv[1]) == ERROR) return ERROR;
	init();
	get_track_bound_box(track_id, min, max);

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE);
	glutInitWindowSize(max[0] + min[0], max[1] + min[1]);
	glutCreateWindow("car model test");

	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutTimerFunc(20, animate, 0);
	glutKeyboardFunc(keyPressed);
	glutKeyboardUpFunc(keyReleased);
	glutSpecialFunc(arrowPressed);
	glutSpecialUpFunc(arrowReleased);
	glutMainLoop();

	return 0;
}
