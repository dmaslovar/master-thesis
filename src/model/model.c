#include <math.h>
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <model.h>

#define CAR_AXIS 2

#define CAR_MASS 10.0
#define CAR_LEN 25.0
#define CAR_WID 15.0
#define CAR_LEN_H 12.5
#define CAR_WID_H 7.5

#define CAR_TRESH 0.01

#define CAR_ACC_FRIC 0.1
#define CAR_FREE_FRIC 1
#define CAR_FRIC_TRESH 0.1
#define CAR_ACC_TRESH 0.05

#define CAR_MAX_VEL 3.0

#define CAR_MAX_CNT 8
#define TRACK_MAX_CNT 8

#define ZERO_TRESH 0.0001

struct _car_model {
	double input[CAR_INS];
	double mass;
	double len;
	double wid;
	double vel;
	double ang;
	double unit;
	double pos[CAR_AXIS];
	double ori[CAR_AXIS];
	double compl;
	int crashed;
	int lap;
};

struct _temp_vals {
	double fcpf[2];
	double fcpr[2];
	double inertC;
	double inertB;
	double radW;
	double inertW;
};

struct _track_wall {
	double start[2];
	double end[2];
};

struct _track_model {
	int wall_size;
	struct _track_wall *walls;
	struct _track_wall *finish;
	int check_size;
	double **checkpoints;
	double *checkpt_weights;
	int max_cars;
	double **start_pos;
	double **start_ori;
	double diag;
};

struct _track_model _tracks[TRACK_MAX_CNT];

int _car_count[TRACK_MAX_CNT] = {0};
struct _car_model _cars[TRACK_MAX_CNT][CAR_MAX_CNT];
struct _temp_vals _move_vals[TRACK_MAX_CNT][CAR_MAX_CNT];
int _track_count = 0;

double _signum(double x) {
	return (x >= 0) - (x < 0);
}

double _min(double a, double b) {
	if (a < b) return a;
	return b;
}

double _max(double a, double b) {
	if (a > b) return a;
	return b;
}

double _point_dist(double *a, double *b) {
	return sqrt(pow(a[0] - b[0], 2) + pow(a[1] - b[1], 2));
}

double _norm(double *v) {
	return sqrt(pow(v[0], 2) + pow(v[1], 2));
}

void _normalise(double *v) {
	double n = _norm(v);
	v[0] /= n; v[1] /= n;
}

double _avoid_zero(double x) {
	if (abs(x) > ZERO_TRESH) return x;

	if (x >= 0) return ZERO_TRESH;
	return -ZERO_TRESH;
}

/*
	t[0] na r
	t[1] na b-a
*/
void _get_intersection_params(double *a, double *b, double *r, double *p, double *t) {
	t[1] = (r[0] * (a[1] - p[1]) - r[1] * (a[0] - p[0]))
			/ _avoid_zero(r[1] * (b[0] - a[0]) - r[0] * (b[1] - a[1]));

	if (abs(r[0]) > ZERO_TRESH)
		t[0] = (a[0] + t[1] * (b[0] - a[0]) - p[0]) / r[0];
	else
		t[0] = (a[1] + t[1] * (b[1] - a[1]) - p[1]) / r[1];
}

void _get_line_intersection_params(double *a, double *b, double *c, double *d, double *t) {
	double r[2];
	r[0] = d[0] - c[0];
	r[1] = d[1] - c[1];
	_get_intersection_params(a, b, r, c, t);
}

double _get_wall_intersection_param(double *a, double *b, double *p) {
	double ray[2], t[2];
	ray[0] = a[1] - b[1]; ray[1] = b[0] - a[0];

	_get_intersection_params(a, b, ray, p, t);
	return t[1];
}

double _get_norm_intersection_param(double *a, double *b, double *p) {
	double t = _get_wall_intersection_param(a, b, p);
	if (t < 0) t = 0;
	if (t > 1) t = 1;

	return t;
}

void _get_intersection_pos(double *a, double *b, double *p, double t, double *x) {
	x[0] = a[0] + t * (b[0] - a[0]);
	x[1] = a[1] + t * (b[1] - a[1]);
}

double _get_intersection_dist(double *a, double *b, double *p, double t) {
	double intersec[2];
	_get_intersection_pos(a, b, p, t, intersec);
	return _point_dist(p, intersec);
}

double _track_compl(int track_id, double *p) {
	int best_index = 0;
	double best_t = 0, t;
	double best_dist = DBL_MAX, dist;
	double *start, *end;
	int chckpt_s = _tracks[track_id].check_size;

	for (int i = 0; i < chckpt_s; i++) {
		start = _tracks[track_id].checkpoints[i];
		end = _tracks[track_id].checkpoints[(chckpt_s + i + 1) % chckpt_s];
		t = _get_norm_intersection_param(start, end, p);
		dist = _get_intersection_dist(start, end, p, t);

		if (dist < best_dist) {
			best_dist = dist;
			best_t = t;
			best_index = i;
		}
	}

	return _tracks[track_id].checkpt_weights[best_index]
		+ (_tracks[track_id].checkpt_weights[best_index + 1] - _tracks[track_id].checkpt_weights[best_index]) * best_t;
}

int init_car(int track_id, double unit)  {
	if (track_id < 0 || track_id >= _track_count) {
		fprintf(stderr, "init_car: track index (%d) out of range (%d)\n", track_id, _track_count - 1);
		return -1;
	}
	if (_car_count[track_id] == _tracks[track_id].max_cars) {
		fprintf(stderr, "Cannot create new cars, car limit (%d) reached\n", _tracks[track_id].max_cars);
		return -1;
	}
	for (int i = 0; i < CAR_INS; i++) _cars[track_id][_car_count[track_id]].input[i] = 0.0;
	_cars[track_id][_car_count[track_id]].mass = CAR_MASS;
	_cars[track_id][_car_count[track_id]].len = CAR_LEN;
	_cars[track_id][_car_count[track_id]].wid = CAR_WID;
	_cars[track_id][_car_count[track_id]].vel = 0.0;
	_cars[track_id][_car_count[track_id]].unit = unit;
	_cars[track_id][_car_count[track_id]].crashed = 0;
	for (int i = 0; i < CAR_AXIS; i++) {
		_cars[track_id][_car_count[track_id]].pos[i] = _tracks[track_id].start_pos[_car_count[track_id]][i];
		_cars[track_id][_car_count[track_id]].ori[i] = _tracks[track_id].start_ori[_car_count[track_id]][i];
	}

	_cars[track_id][_car_count[track_id]].compl = _track_compl(track_id,
															_cars[track_id][_car_count[track_id]].pos);
	if (_cars[track_id][_car_count[track_id]].compl > 0.5)
		_cars[track_id][_car_count[track_id]].lap = -1;
	else
		_cars[track_id][_car_count[track_id]].lap = 0;

	_move_vals[track_id][_car_count[track_id]].inertC = CAR_MASS * (pow(CAR_WID, 2) + pow(CAR_LEN, 2)) / 12.0;
	_move_vals[track_id][_car_count[track_id]].inertB = _move_vals[track_id][_car_count[track_id]].inertC + CAR_MASS * pow(CAR_LEN / 2.0, 2);

	return _car_count[track_id]++;
}

void set_car_input(int track_id, int car_id, int in_id, double val) {
	if (track_id < 0 || track_id >= _track_count) {
		fprintf(stderr, "set_car_input: track index (%d) out of range (%d)\n", track_id, _track_count - 1);
		return;
	}
	if (car_id < 0 || car_id >= _car_count[track_id]) {
		fprintf(stderr, "set_car_input: car index (%d) out of range (%d)\n", car_id, _car_count[track_id] - 1);
		return;
	}
	_cars[track_id][car_id].input[in_id] = val;
}

double get_track_completion(int track_id, int car_id) {
	if (track_id < 0 || track_id >= _track_count) {
		fprintf(stderr, "get_track_completion: track index (%d) out of range (%d)\n", track_id, _track_count - 1);
		return -1;
	}
	if (car_id < 0 || car_id >= _car_count[track_id]) {
		fprintf(stderr, "get_track_completion: car index (%d) out of range (%d)\n", car_id, _car_count[track_id] - 1);
		return -1;
	}

	if (_cars[track_id][car_id].lap < 0) return 0;

	return _cars[track_id][car_id].lap + _cars[track_id][car_id].compl;
}

void _move_car(int track_id, int car_id, double dt) {
	double new_pos[2];
	double in_steer = 1.5 * _cars[track_id][car_id].input[CAR_STEER];
	double in_accel = _cars[track_id][car_id].input[CAR_ACCEL];
	double sgn = _signum(_cars[track_id][car_id].vel);
	double fric = sgn * CAR_ACC_FRIC;
	double compl;

	double omega = _cars[track_id][car_id].vel / _cars[track_id][car_id].len * tan(in_steer);
	if (fabs(in_accel) < CAR_ACC_TRESH) {
		in_accel = 0.0;
		if (fabs(_cars[track_id][car_id].vel) < CAR_FRIC_TRESH) {
			fric = 0.0;
			_cars[track_id][car_id].vel = 0.0;
		} else {
			fric = sgn * CAR_FREE_FRIC;
		}
	}

	double ang = 1.5 * omega * dt;
	double new_x = _cars[track_id][car_id].ori[0] * cos(ang) - _cars[track_id][car_id].ori[1] * sin(ang);
	_cars[track_id][car_id].ori[1] = _cars[track_id][car_id].ori[0] * sin(ang) +
									_cars[track_id][car_id].ori[1] * cos(ang);
	_cars[track_id][car_id].ori[0] = new_x;
	_normalise(_cars[track_id][car_id].ori);

	double acc = in_accel - fric;
	double ds = _cars[track_id][car_id].unit * (_cars[track_id][car_id].vel * dt + acc * dt * dt / 2);
	_cars[track_id][car_id].vel = _max(_min(_cars[track_id][car_id].vel + acc * dt, CAR_MAX_VEL), -CAR_MAX_VEL);

	new_pos[0] = _cars[track_id][car_id].pos[0] + ds * _cars[track_id][car_id].ori[0];
	new_pos[1] = _cars[track_id][car_id].pos[1] + ds * _cars[track_id][car_id].ori[1];

	compl = _track_compl(track_id, new_pos);

	if (compl - _cars[track_id][car_id].compl > 0.9) {
		if (_cars[track_id][car_id].lap >= 0)
			_cars[track_id][car_id].lap--;
	} else if (_cars[track_id][car_id].compl - compl > 0.9) {
		_cars[track_id][car_id].lap++;
	}

	_cars[track_id][car_id].compl = compl;

	_cars[track_id][car_id].pos[0] = new_pos[0];
	_cars[track_id][car_id].pos[1] = new_pos[1];
}

void _fill_car_body(int track_id, int id, double (*body)[2]) {
	double front[2], side[2];
	struct _car_model *car = _cars[track_id] + id;

	front[0] = CAR_LEN_H * car->ori[0];
	front[1] = CAR_LEN_H * car->ori[1];

	side[0] = -CAR_WID_H * car->ori[1];
	side[1] = CAR_WID_H * car->ori[0];

	body[0][0] = car->pos[0] + front[0] + side[0];
	body[0][1] = car->pos[1] + front[1] + side[1];

	body[1][0] = car->pos[0] - front[0] + side[0];
	body[1][1] = car->pos[1] - front[1] + side[1];

	body[2][0] = car->pos[0] - front[0] - side[0];
	body[2][1] = car->pos[1] - front[1] - side[1];

	body[3][0] = car->pos[0] + front[0] - side[0];
	body[3][1] = car->pos[1] + front[1] - side[1];
}

void _check_track_collision(int track_id, int id) {
	double body[4][2];
	double t[2];

	_fill_car_body(track_id, id, body);

	for (int i = 0; i < _tracks[track_id].wall_size; i++)
		for (int j = 0; j < 4; j++) {
			_get_line_intersection_params(_tracks[track_id].walls[i].start, _tracks[track_id].walls[i].end,
											body[j], body[(j + 1) % 4], t);
			if (t[0] >= 0.0 && t[0] <= 1.0 && t[1] >= 0.0 && t[1] <= 1.0) {
				_cars[track_id][id].crashed = 1;
				//printf("car %d crashed with wall %d at t=[%.2lf, %.2lf]\n", id, i, t[0], t[1]);
				return;
			}
		}
}

void _check_car_collision(int track_id, int id_a, int id_b) {
	double body_a[4][2], body_b[4][2], t[2];

	_fill_car_body(track_id, id_a, body_a);
	_fill_car_body(track_id, id_b, body_b);

	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++) {
			_get_line_intersection_params(body_a[i], body_a[(i + 1) % 4], body_b[j], body_b[(j + 1) % 4], t);
			if (t[0] >= 0.0 && t[0] <= 1.0 && t[1] >= 0.0 && t[1] <= 1.0) {
				_cars[track_id][id_a].crashed = 1;
				_cars[track_id][id_b].crashed = 1;
				//printf("cars %d and %d crashed\n", id_a, id_b);
				return;
			}
		}
}

void move_cars(int track_id, double dt) {
	if (track_id < 0 || track_id >= _track_count) {
		fprintf(stderr, "move_cars: track index (%d) out of range (%d)\n", track_id, _track_count - 1);
		return;
	}
	if (dt < 0) {
		fprintf(stderr, "move_cars: dt cannot be negative (%lf)\n", dt);
		return;
	}

	for (int i = 0; i < _car_count[track_id]; i++)
		if (!_cars[track_id][i].crashed)
			_move_car(track_id, i, dt);

	for (int i = 0; i < _car_count[track_id]; i++)
		if (!_cars[track_id][i].crashed)
			_check_track_collision(track_id, i);

	for (int i = 0; i < _car_count[track_id]; i++)
		for (int j = i + 1; j < _car_count[track_id]; j++)
			_check_car_collision(track_id, i, j);

}

int all_cars_crashed(int track_id) {
	if (track_id < 0 || track_id >= _track_count) {
		fprintf(stderr, "all_cars_crashed: track index (%d) out of range (%d)\n", track_id, _track_count - 1);
		return 0;
	}
	for (int i = 0; i < _car_count[track_id]; i++)
		if (!_cars[track_id][i].crashed) return 0;

	return 1;
}

int car_crashed(int track_id, int car_id) {
	if (track_id < 0 || track_id >= _track_count) {
		fprintf(stderr, "car_crashed: track index (%d) out of range (%d)\n", track_id, _track_count - 1);
		return -1;
	}
	if (car_id < 0 || car_id >= _car_count[track_id]) {
		fprintf(stderr, "car_crashed: car index (%d) out of range (%d)\n", car_id, _car_count[track_id] - 1);
		return -1;
	}
	return _cars[track_id][car_id].crashed;
}

double get_car_angle(int track_id, int car_id) {
	if (track_id < 0 || track_id >= _track_count) {
		fprintf(stderr, "get_car_angle: track index (%d) out of range (%d)\n", track_id, _track_count - 1);
		return -1;
	}
	if (car_id < 0 || car_id >= _car_count[track_id]) {
		fprintf(stderr, "get_car_angle: car index (%d) out of range (%d)\n", car_id, _car_count[track_id] - 1);
		return -1;
	}
	return atan2(_cars[track_id][car_id].ori[1], _cars[track_id][car_id].ori[0]);
}

void get_car_position(int track_id, int car_id, double *p) {
	if (track_id < 0 || track_id >= _track_count) {
		fprintf(stderr, "get_car_position: track index (%d) out of range (%d)\n", track_id, _track_count - 1);
		return;
	}
	if (car_id < 0 || car_id >= _car_count[track_id]) {
		fprintf(stderr, "get_car_position: car index (%d) out of range (%d)\n", car_id, _car_count[track_id] - 1);
		return;
	}
	p[0] = _cars[track_id][car_id].pos[0];
	p[1] = _cars[track_id][car_id].pos[1];
}

double get_car_velocity(int track_id, int car_id) {
	if (track_id < 0 || track_id >= _track_count) {
		fprintf(stderr, "get_car_velocity: track index (%d) out of range (%d)\n", track_id, _track_count - 1);
		return -1;
	}
	if (car_id < 0 || car_id >= _car_count[track_id]) {
		fprintf(stderr, "get_car_velocity: car index (%d) out of range (%d)\n", car_id, _car_count[track_id] - 1);
		return -1;
	}
	return _cars[track_id][car_id].vel;
}

void draw_car(int track_id, int car_id, void (*draw_quad_point)(double, double)) {
	double body[4][2];
	if (track_id < 0 || track_id >= _track_count) {
		fprintf(stderr, "draw_car: track index (%d) out of range (%d)\n", track_id, _track_count - 1);
		return;
	}
	if (car_id < 0 || car_id >= _car_count[track_id]) {
		fprintf(stderr, "draw_car: car index (%d) out of range (%d)\n", car_id, _car_count[track_id] - 1);
		return;
	}
	_fill_car_body(track_id, car_id, body);

	draw_quad_point(body[0][0], body[0][1]);
	draw_quad_point(body[1][0], body[1][1]);
	draw_quad_point(body[2][0], body[2][1]);
	draw_quad_point(body[3][0], body[3][1]);
}

int get_closest_checkpt(int track_id, double *p) {
	int min_i;
	double min_d = DBL_MAX, d;

	if (track_id < 0 || track_id >= _track_count) {
		fprintf(stderr, "get_closest_checkpt: track index (%d) out of range (%d)\n", track_id, _track_count - 1);
		return -1;
	}

	for (int i = 0; i < _tracks[track_id].check_size; i++) {
		d = _point_dist(p, _tracks[track_id].checkpoints[i]);

		if (d < min_d) {
			min_i = i;
			min_d = d;
		}
	}

	return min_i;
}

void get_closest_point(int track_id, double *p, double *x) {
	int best_index = 0;
	double best_t = 0, t;
	double best_dist = DBL_MAX, dist;
	double *start, *end;

	if (track_id < 0 || track_id >= _track_count) {
		fprintf(stderr, "get_closest_point: track index (%d) out of range (%d)\n", track_id, _track_count - 1);
		return;
	}

	int chckpt_s = _tracks[track_id].check_size;

	for (int i = 0; i < chckpt_s; i++) {
		start = _tracks[track_id].checkpoints[i];
		end = _tracks[track_id].checkpoints[(chckpt_s + i + 1) % chckpt_s];
		t = _get_norm_intersection_param(start, end, p);
		dist = _get_intersection_dist(start, end, p, t);

		if (dist < best_dist) {
			best_dist = dist;
			best_t = t;
			best_index = i;
		}
	}

	start = _tracks[track_id].checkpoints[best_index];
	end = _tracks[track_id].checkpoints[(best_index + 1) % chckpt_s];
	_get_intersection_pos(start, end, p, best_t, x);
}

double next_chckpt_ang(int track_id, int car_id) {
	if (track_id < 0 || track_id >= _track_count) {
		fprintf(stderr, "next_chckpt_ang: track index (%d) out of range (%d)\n", track_id, _track_count - 1);
		return -1;
	}
	if (car_id < 0 || car_id >= _car_count[track_id]) {
		fprintf(stderr, "next_chckpt_ang: car index (%d) out of range (%d)\n", car_id, _car_count[track_id] - 1);
		return -1;
	}

	int best_index = 0;
	double t;
	double best_dist = DBL_MAX, dist;
	double *start, *end, *p = _cars[track_id][car_id].pos, *o = _cars[track_id][car_id].ori;
	int chckpt_s = _tracks[track_id].check_size;
	double car_chckpt[2];
	double sin_ang, cos_ang;

	for (int i = 0; i < chckpt_s; i++) {
		start = _tracks[track_id].checkpoints[i];
		end = _tracks[track_id].checkpoints[(chckpt_s + i + 1) % chckpt_s];
		t = _get_norm_intersection_param(start, end, p);
		dist = _get_intersection_dist(start, end, p, t);

		if (dist < best_dist) {
			best_dist = dist;
			best_index = i;
		}
	}

	best_index = (best_index + 1) % _tracks[track_id].check_size;

	for (int i = 0; i < CAR_AXIS; i++)
		car_chckpt[i] = _tracks[track_id].checkpoints[best_index][i] - p[i];

	sin_ang = asin((car_chckpt[1] * o[0] - car_chckpt[0] * o[1]) / _norm(car_chckpt) / _norm(o));
	cos_ang = acos((car_chckpt[0] * o[0] + car_chckpt[1] * o[1]) / _norm(car_chckpt) / _norm(o));

	if (sin_ang >= 0)
		return cos_ang;
	else
		return -cos_ang;
}

double *_get_checkpt_weights(int check_size, double **checkpts) {
	int w_size = check_size + 1;
	double *weights = (double*) malloc(w_size * sizeof(double));

	weights[0] = 0;
	for (int i = 1; i < w_size; i++)
		weights[i] = weights[i -1] + _point_dist(checkpts[i % check_size], checkpts[i-1]);
	for (int i = 0; i < w_size; i++)
		weights[i] /= weights[check_size];

	return weights;
}

void _get_wall_bb(double *min, double *max, int wall_cnt, struct _track_wall *walls) {
	for (int i = 0; i < 2; i++) {
		min[i] = DBL_MAX;
		max[i] = 0;
	}

	for (int i = 0; i < wall_cnt; i++) {
		if (walls[i].start[0] > max[0]) max[0] = walls[i].start[0]; //x
		if (walls[i].start[1] > max[1]) max[1] = walls[i].start[1]; //y
		if (walls[i].start[0] < min[0]) min[0] = walls[i].start[0]; //x
		if (walls[i].start[1] < min[1]) min[1] = walls[i].start[1]; //y

		if (walls[i].end[0] > max[0]) max[0] = walls[i].end[0]; //x
		if (walls[i].end[1] > max[1]) max[1] = walls[i].end[1]; //y
		if (walls[i].end[0] < min[0]) min[0] = walls[i].end[0]; //x
		if (walls[i].end[1] < min[1]) min[1] = walls[i].end[1]; //y
	}
}

void get_track_bound_box(int track_id, double *min, double *max) {
	_get_wall_bb(min, max, _tracks[track_id].wall_size, _tracks[track_id].walls);
}

double _get_diag(int wall_cnt, struct _track_wall *walls) {
	double min[2], max[2];
	_get_wall_bb(min, max, wall_cnt, walls);

	return _point_dist(max, min);
}

void _fill_track(struct _track_model *result, int wall_size, struct _track_wall *walls,
		struct _track_wall *finish, int check_size, double **checkpoints, double max_cars,
		double **start_pos, double **start_ori) {

	result->wall_size = wall_size;
	result->walls = walls;
	result->finish = finish;
	result->check_size = check_size;
	result->checkpoints = checkpoints;
	result->max_cars = max_cars;
	result->start_pos = start_pos;
	result->start_ori = start_ori;
	result->diag = _get_diag(wall_size, walls);

	result->checkpt_weights = _get_checkpt_weights(check_size, checkpoints);
}

int load_track(char *filename) {
	FILE *file;
	int wall_size, check_size, max_cars;
	struct _track_wall *walls, *finish;
	double **checkpoints, *vec, **start_pos, **start_ori;

	if (_track_count >= TRACK_MAX_CNT) {
		fprintf(stderr, "load_track: maximum number of tracks already reached: %d\n", _track_count);
		return -1;
	}

	if ((file = fopen(filename, "r")) == 0) {
		fprintf(stderr, "Cannot open file %s: %s\n", filename, strerror(errno));
		return -1;
	}

	fscanf(file, "%d", &wall_size);
	walls = (struct _track_wall *) malloc(wall_size * sizeof(struct _track_wall));
	for (int i = 0; i < wall_size; i++) {
		fscanf(file, "%lf %lf", &(walls[i].start[0]), &(walls[i].start[1]));
		fscanf(file, "%lf %lf", &(walls[i].end[0]), &(walls[i].end[1]));
	}
	finish = (struct _track_wall *) malloc(sizeof(struct _track_wall));
	fscanf(file, "%lf %lf", finish->start, finish->start + 1);
	fscanf(file, "%lf %lf", finish->end, finish->end + 1);

	fscanf(file, "%d", &check_size);
	checkpoints = (double**) malloc(check_size * sizeof(double*));
	for (int i = 0; i < check_size; i++) {
		vec = (double*) malloc(2 * sizeof(double));
		fscanf(file, "%lf %lf", vec, vec + 1);
		checkpoints[i] = vec;
	}

	fscanf(file, "%d", &max_cars);
	start_pos = (double**) malloc(max_cars * sizeof(double*));
	start_ori = (double**) malloc(max_cars * sizeof(double*));
	for (int i = 0; i < max_cars; i++) {
		vec = (double*) malloc(2 * sizeof(double));
		fscanf(file, "%lf %lf", vec, vec + 1);
		start_pos[i] = vec;

		vec = (double*) malloc(2 * sizeof(double));
		fscanf(file, "%lf %lf", vec, vec + 1);
		start_ori[i] = vec;
	}

	fclose(file);
	_fill_track(&(_tracks[_track_count]), wall_size, walls, finish, check_size, checkpoints,
				max_cars, start_pos, start_ori);
	return _track_count++;
}

void draw_track(int track_id, void (*draw_line)(double, double, double, double)) {
	if (track_id < 0 || track_id >= _track_count) {
		fprintf(stderr, "draw_track: track index (%d) out of range (%d)\n", track_id, _track_count - 1);
		return;
	}
	for (int i = 0; i < _tracks[track_id].wall_size; i++)
		(*draw_line)(_tracks[track_id].walls[i].start[0], _tracks[track_id].walls[i].start[1],
					_tracks[track_id].walls[i].end[0], _tracks[track_id].walls[i].end[1]);
}

int get_chckpt_size(int track_id) {
	if (track_id < 0 || track_id >= _track_count) {
		fprintf(stderr, "get_chckpt_size: track index (%d) out of range (%d)\n", track_id, _track_count - 1);
		return -1;
	}
	return _tracks[track_id].check_size;
}

void get_chckpt(int track_id, int index, double *x) {
	if (track_id < 0 || track_id >= _track_count) {
		fprintf(stderr, "get_chchkpt: track index (%d) out of range (%d)\n", track_id, _track_count - 1);
		return;
	}
	if (index < 0 || index >= _tracks[track_id].check_size) {
		fprintf(stderr, "checkpoints index (%d) out of range (%d)\n", index, _tracks[track_id].check_size);
		return;
	}
	x[0] = _tracks[track_id].checkpoints[index][0];
	x[1] = _tracks[track_id].checkpoints[index][1];
}

double get_wall_dist(int track_id, int car_id, double rad) {
	if (track_id < 0 || track_id >= _track_count) {
		fprintf(stderr, "get_wall_dist: track index (%d) out of range (%d)\n", track_id, _track_count - 1);
		return -1;
	}
	if (car_id < 0 || car_id >= _car_count[track_id]) {
		fprintf(stderr, "get_wall_dist: car index (%d) out of range (%d)\n", car_id, _car_count[track_id] - 1);
		return -1;
	}

	double t_min = DBL_MAX, ang = get_car_angle(track_id, car_id) + rad;
	double ray[2], t[2];

	ray[0] = cos(ang);
	ray[1] = sin(ang);

	for (int i = 0; i < _tracks[track_id].wall_size; i++) {
		_get_intersection_params(_tracks[track_id].walls[i].start, _tracks[track_id].walls[i].end,
								ray, _cars[track_id][car_id].pos, t);
		if (t[1] >= 0 && t[1] <= 1 && t[0] > 0 && t[0] < t_min)
			t_min = t[0];
	}

	return t_min / _tracks[track_id].diag;
}

double get_car_dist(int track_id, int car_id, double rad) {
	if (track_id < 0 || track_id >= _track_count) {
		fprintf(stderr, "get_wall_dist: track index (%d) out of range (%d)\n", track_id, _track_count - 1);
		return -1;
	}
	if (car_id < 0 || car_id >= _car_count[track_id]) {
		fprintf(stderr, "get_wall_dist: car index (%d) out of range (%d)\n", car_id, _car_count[track_id] - 1);
		return -1;
	}

	double t_min = DBL_MAX, ang = get_car_angle(track_id, car_id) + rad;
	double ray[2], t[2], body[4][2];

	ray[0] = cos(ang);
	ray[1] = sin(ang);

	for (int i = 0; i < _car_count[track_id]; i++) {
		if (i == car_id) continue;
		_fill_car_body(track_id, i, body);
		for (int j = 0; j < 4; j++) {
			_get_intersection_params(body[j], body[(j+1) % 4], ray, _cars[track_id][car_id].pos, t);
			if (t[1] >= 0 && t[1] <= 1 && t[0] > 0 && t[0] < t_min)
				t_min = t[0];
		}
	}


	if (t_min == DBL_MAX) return 1.0;

	return t_min / _tracks[track_id].diag;
}

void clear_cars(int track_id) {
	if (track_id < 0 || track_id >= _track_count) {
		fprintf(stderr, "clear_cars: track index (%d) out of range (%d)\n", track_id, _track_count - 1);
		return;
	}
	_car_count[track_id] = 0;
}

int conf_load_tracks(struct track_ids **identifiers, int *n, struct configuration *config) {
	struct track_ids *ids;
	int track_num, car_num;
	double unit;
	char **track_names;

	if (get_config_integer(config, _MODEL_CAR_NUM, &car_num) == CONF_ERR) return -1;
	if (get_config_double(config, _MODEL_CAR_UNIT, &unit) == CONF_ERR) return -1;
	if (get_config_integer(config, _MODEL_TRACK_NUM, &track_num) == CONF_ERR) return -1;
	if (get_config_string_array(config, _MODEL_TRACK_FILE, track_num, &track_names) == CONF_ERR) return -1;

	ids = (struct track_ids*) malloc(track_num * sizeof(struct track_ids));

	for (int i = 0; i < track_num; i++) {
		if ((ids[i].track_id = load_track(track_names[i])) < 0) return -1;
		ids[i].car_n = car_num;
		ids[i].car_unit = unit;
		ids[i].car_ids = (int*) malloc(car_num * sizeof(int));
	}

	*identifiers = ids;
	*n = track_num;

	return 0;
}

void release_track_ids(int n, struct track_ids *ids) {
	for (int i = 0; i < n; i++)
		free(ids->car_ids);
	free(ids);
}
