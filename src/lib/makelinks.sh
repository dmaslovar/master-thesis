#! /bin/bash
DIRS=(../util/gauss ../util/unit_test ../util/conf ../evol ../neur ../model ../fit)
LIBS=(libgauss.a libtest.a libconfig.a libevol.a libneur.a libmodel.a libfit.a)
for (( i = 0; i<${#LIBS[@]}; i++ )); do
	file=${DIRS[$i]}/${LIBS[$i]}
	if [ ! -f $file ]; then
		echo "$file does not exist, compiling"
		cd ${DIRS[$i]}
		make lib
		cd -
	fi

	if [ ! -f $file ]; then
		echo "$file not compiled, skipping"
	else
		ln -fs $file .
	fi
done;
