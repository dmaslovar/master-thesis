#ifndef _NEUR_NET_H
#define _NEUR_NET_H

#include <config.h>

#define _NN_ARCH_LEN "architecture.length"
#define _NN_ARCH "architecture"
#define _NN_HID_ACT "architecture.hidden.activation"
#define _NN_OUT_ACT "architecture.output.activation"

#define _NN_ACT_SIGM "sigmoid"
#define _NN_ACT_RELU "relu"
#define _NN_ACT_TANH "tanh"

struct neural_net {
	int n_lay;
	int *arch;
	double (*hid_act)(double);
	double (*out_act)(double);
	double **_inter;
};

double sigmoid(double x);

double relu(double x);

struct neural_net *new_network(int n_layers, int *architecture, double (*hid_act)(double), double (*out_act)(double));

void release_neur_net(struct neural_net *net);

int input_size(struct neural_net *net);

int output_size(struct neural_net *net);

int weights_size(struct neural_net *net);

void eval_neur_net(struct neural_net *net, double *weights, double *input, double *output);

int write_net_weights(char *filename, struct neural_net *net, double *weights);

double *load_net_weights(char *filename, struct neural_net *net);

struct neural_net *conf_load_neur_net(struct configuration *config);

#endif
