#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <neur_net.h>

typedef double (*_activation_function)(double);

double sigmoid(double x) {
	return 1.0 / (1.0 + pow(M_E, -x));
}

double relu(double x) {
	if (x < 0) return 0;
	return x;
}

struct neural_net *new_network(int n_layers, int *architecture, double (*hid_act)(double), double (out_act)(double)) {
	struct neural_net *result = (struct neural_net*) malloc(sizeof(struct neural_net));

	result->n_lay = n_layers;
	result->arch = (int*) malloc(n_layers * sizeof(int));
	result->hid_act = hid_act;
	result->out_act = out_act;
	memcpy(result->arch, architecture, n_layers * sizeof(int));

	result->_inter = (double**) malloc(n_layers * sizeof(double*));
	for (int i = 1; i < n_layers - 1; i++)
		result->_inter[i] = (double*) malloc(architecture[i] * sizeof(double));

	return result;
}

void release_neur_net(struct neural_net *net) {
	for (int i = 1, n = net->n_lay - 1; i < n; i++)
		free(net->_inter[i]);
	free(net->_inter);
	free(net->arch);
	free(net);
}

int input_size(struct neural_net *net) {
	return net->arch[0];
}

int output_size(struct neural_net *net) {
	return net->arch[net->n_lay  - 1];
}

int weights_size(struct neural_net *net) {
	int res = 0;
	for (int i = 0; i < net->n_lay - 1; i++)
		res += net->arch[i + 1] * (net->arch[i] + 1);

	return res;
}

void _layer_propagation(double *weights, int w_off, double *in, int in_size, double *out, int out_size, double (*act)(double)) {
	for (int i = 0, w = w_off; i < out_size; i++, w += (in_size + 1)) {
		out[i] = weights[w];

		for (int j = 0; j < in_size; j++)
			out[i] += weights[w + 1 + j] * in[j];

		out[i] = act(out[i]);
	}
}

void eval_neur_net(struct neural_net *net, double *weights, double *input, double *output) {
	int w_off = 0, i;
	net->_inter[0] = input;
	net->_inter[net->n_lay - 1] = output;

	for (i = 0; i < net->n_lay - 2; i++) {
		_layer_propagation(weights, w_off, net->_inter[i], net->arch[i], net->_inter[i + 1], net->arch[i + 1], net->hid_act);
		w_off += (net->arch[i] + 1) * net->arch[i + 1];
	}
	_layer_propagation(weights, w_off, net->_inter[i], net->arch[i], net->_inter[i + 1], net->arch[i + 1], net->out_act);
}

int write_net_weights(char *filename, struct neural_net *net, double *weights) {
	FILE *f;
	int w_len = weights_size(net);

	if ((f = fopen(filename, "w")) == NULL) {
		fprintf(stderr, "Cannot write to file %s: %s\n", filename, strerror(errno));
		return -1;
	}

	fprintf(f, "%d\n", net->n_lay);
	for (int i = 0; i < net->n_lay; i++)
		fprintf(f, "%d ", net->arch[i]);
	fprintf(f, "\n");

	for (int i = 0; i < w_len; i++)
		fprintf(f, "%lf ", weights[i]);

	fclose(f);

	return 0;
}

double *load_net_weights(char *filename, struct neural_net *net) {
	FILE *f;
	int lays, *arch, w_len;
	double *weights;

	if ((f = fopen(filename, "r")) == NULL) {
		fprintf(stderr, "Cannot read file %s: %s\n", filename, strerror(errno));
		return NULL;
	}

	fscanf(f, "%d", &lays);
	arch = (int*) malloc(lays * sizeof(int));
	for (int i = 0; i < lays; i++)
		fscanf(f, "%d", arch + i);

	net->n_lay = lays;
	net->arch = arch;
	net->hid_act = sigmoid;
	net->out_act = sigmoid;

	net->_inter = (double**) malloc(lays * sizeof(double*));
	for (int i = 1; i < lays - 1; i++)
		net->_inter[i] = (double*) malloc(arch[i] * sizeof(double));

	w_len = weights_size(net);
	weights = (double*) malloc(w_len * sizeof(double));
	for (int i = 0; i < w_len; i++)
		fscanf(f, "%lf", weights + i);

	fclose(f);

	return weights;
}

_activation_function _get_act(char *act_name, struct configuration *config) {
	char *buf;

	if (get_config_value_err(config, act_name, &buf) == CONF_ERR) return NULL;

	if (strcmp(buf, _NN_ACT_SIGM) == 0)
		return sigmoid;
	else if (strcmp(buf, _NN_ACT_RELU) == 0)
		return relu;
	else if (strcmp(buf, _NN_ACT_TANH) == 0)
		return tanh;

	fprintf(stderr, "unknown activation: %s\n", buf);
	return NULL;
}

struct neural_net *conf_load_neur_net(struct configuration *config) {
	int n_lay, *arch;
	double (*hid_act)(double), (*out_act)(double);

	if (get_config_integer(config, _NN_ARCH_LEN, &n_lay) == CONF_ERR) return NULL;
	if (get_config_integer_array(config, _NN_ARCH, n_lay, &arch) == CONF_ERR) return NULL;

	if ((hid_act = _get_act(_NN_HID_ACT, config)) == NULL) return NULL;
	if ((out_act = _get_act(_NN_OUT_ACT, config)) == NULL) return NULL;

	return new_network(n_lay, arch, hid_act, out_act);
}
