#include <stdio.h>
#include <neur_net.h>
#include <test.h>

#define N_FUN 4
#define ERR_TRESH 0.01

#define INS 2
#define OUTS 1
#define W_SIZE 20

int architecture[] = {INS, 3, 2, OUTS};
struct neural_net *net;
double weights[W_SIZE];

void test_ins() {
	test_condition(input_size(net) == INS);
}

void test_outs() {
	test_condition(output_size(net) == OUTS);
}

void test_weights() {
	test_condition(weights_size(net) == W_SIZE);
}

void test_evaluation() {
	double input[INS] = {0.5, 0.6}, output;
	eval_neur_net(net, weights, input, &output);
	test_doubles(output, 0.91, ERR_TRESH);
}

void global_init() {
	net = new_network(4, architecture, sigmoid, sigmoid);

	for (int i = 0; i < W_SIZE; i++)
		weights[i] = (i % 10 + 1) * 0.1;
}

int main(int argc, char **argv) {
	void (*functions[N_FUN])();

	global_init();

	functions[0] = test_ins;
	functions[1] = test_outs;
	functions[2] = test_weights;
	functions[3] = test_evaluation;

	return run_tests(functions, N_FUN);
}
