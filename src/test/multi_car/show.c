#include <math.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <GL/glut.h>
#include "../../model/model.h"
#include "../../neur/neur_net.h"
#include <time.h>

#define STEER 1.5
#define X_CORD 0
#define Y_CORD 1
#define UNIT 50.0
#define AFT_CRASH 100
#define T_STEP 0.02
#define MILLIS 20
#define ERROR 1

#define LEFT_COMM 0
#define RIGHT_COMM 1
#define ACC_COMM 2
#define DECC_COMM 3

struct neural_net *sing_net, *mult_net;
double *sing_weights, *mult_weights;
int step = 0;
int sing_car, mult_car, track_id;
struct timespec glob_start;

double angles[8] = {0, 0.25 * M_PI, 0.5 * M_PI, 0.75 * M_PI, M_PI, 1.25 * M_PI, 1.5 * M_PI, 1.75 * M_PI};

double in[18], out[4];

void draw_quad_point(double x, double y) {
	glVertex2d(x, y);
}

void draw_line(double x1, double y1, double x2, double y2) {
	glVertex2d(x1, y1);
	glVertex2d(x2, y2);
}

void render_scene() {
	glPointSize(1);

	glColor3d(0, 0, 0);
	glBegin(GL_LINES);
	draw_track(track_id, &draw_line);
	glEnd();

	glColor3d(1, 0, 0.3);
	glBegin(GL_QUADS);
	draw_car(track_id, sing_car, &draw_quad_point);
	glEnd();

	glColor3d(0.3, 0, 1);
	glBegin(GL_QUADS);
	draw_car(track_id, mult_car, &draw_quad_point);
	glEnd();
}

void display() {
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();
	render_scene();
	glutSwapBuffers();
}

void reshape(int width, int height) {
	glDisable(GL_DEPTH_TEST);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, width - 1, 0, height - 1, 0, 1);
	glViewport(0, 0,(GLsizei) width, (GLsizei) height);
	glMatrixMode(GL_MODELVIEW);
}

void animate(int value) {
//	struct timespec start, t_in, neur, moved, glt;

	if (all_cars_crashed(track_id)) {
		if (step < AFT_CRASH) {
			step++;
		} else {
			step = 0;
			clear_cars(track_id);
			if (random() % 2 == 0) {
				sing_car = init_car(track_id, UNIT);
				mult_car = init_car(track_id, UNIT);
			} else {
				mult_car = init_car(track_id, UNIT);
				sing_car = init_car(track_id, UNIT);
			}
		}
	} else {
//		clock_gettime(CLOCK_MONOTONIC_RAW, &start);
		in[0] = get_car_velocity(track_id, sing_car);
		in[1] = next_chckpt_ang(track_id, sing_car);
		for (int i = 0; i < 8; i++)
	 		in[i + 2] = get_wall_dist(track_id, sing_car, angles[i]);
//		clock_gettime(CLOCK_MONOTONIC_RAW, &t_in);
		eval_neur_net(sing_net, sing_weights, in, out);
//		clock_gettime(CLOCK_MONOTONIC_RAW, &neur);
		set_car_input(track_id, sing_car, CAR_STEER, STEER * (out[LEFT_COMM] - out[RIGHT_COMM]));
		set_car_input(track_id, sing_car, CAR_ACCEL, out[ACC_COMM] - out[DECC_COMM]);

//		clock_gettime(CLOCK_MONOTONIC_RAW, &start);
		in[0] = get_car_velocity(track_id, mult_car);
		in[1] = next_chckpt_ang(track_id, mult_car);
		for (int i = 0; i < 8; i++) {
	 		in[i + 2] = get_wall_dist(track_id, mult_car, angles[i]);
	 		in[i + 10] = get_car_dist(track_id, mult_car, angles[i]);
	 	}
//		clock_gettime(CLOCK_MONOTONIC_RAW, &t_in);
		eval_neur_net(mult_net, mult_weights, in, out);
//		clock_gettime(CLOCK_MONOTONIC_RAW, &neur);
		set_car_input(track_id, mult_car, CAR_STEER, STEER * (out[LEFT_COMM] - out[RIGHT_COMM]));
		set_car_input(track_id, mult_car, CAR_ACCEL, out[ACC_COMM] - out[DECC_COMM]);

		move_cars(track_id, T_STEP);
//		clock_gettime(CLOCK_MONOTONIC_RAW, &moved);
/*		get_car_position(car_id, pos);
		printf("step %3d; in [", step);

		for (int i = 0; i < 10; i++)
			printf(" %6.3lf", in[i]);

		printf("]; out [%6.3lf %6.3lf]; pos [%6.3lf %6.3lf]\n",
				STEER * (out[LEFT_COMM] - out[RIGHT_COMM]), out[ACC_COMM] - out[DECC_COMM],
				pos[0], pos[1]);
*/
	}
	glutPostRedisplay();
	glutTimerFunc(MILLIS, animate, 0);
/*	clock_gettime(CLOCK_MONOTONIC_RAW, &glt);

	printf("step: %3d; glob_t: %ld; inputs: %ld; neur: %ld; moved: %ld; glut: %ld\n", step,
			(start.tv_sec - glob_start.tv_sec) * 1000 + (start.tv_nsec - glob_start.tv_nsec) / 1000000,
			(t_in.tv_sec - start.tv_sec) * 1000000 + (t_in.tv_nsec - start.tv_nsec) / 1000,
			(neur.tv_sec - start.tv_sec) * 1000000 + (neur.tv_nsec - start.tv_nsec) / 1000,
			(moved.tv_sec - start.tv_sec) * 1000000 + (moved.tv_nsec - start.tv_nsec) / 1000,
			(glt.tv_sec - start.tv_sec) * 1000000 + (glt.tv_nsec - start.tv_nsec) / 1000);
*/}

/*
int print_neur(char *filename) {
	int n;
	FILE *f;

	n = weights_size(net);
	if ((f = fopen(filename, "w")) == 0) {
		fprintf(stderr, "Cannot open file %s: %s\n", filename, strerror(errno));
		return ERROR;
	}

	for (int i = 0; i < n; i++)
		fprintf(f, "%lf ", weights[i]);

	return fclose(f);
}

void run_neur() {
	double out[arch[n_lay - 1]], in[arch[0]], pos[2];

	for (int j = 0; j < 1; j++) {
		in[0] = get_car_velocity(car_id);
		in[1] = next_chckpt_ang(car_id);
		for (int i = 0; i < 8; i++)
	 		in[i + 2] = get_wall_dist(car_id, angles[i]);

		eval_neur_net(net, weights, in, out);
		set_car_input(car_id, CAR_STEER, STEER * (out[LEFT_COMM] - out[RIGHT_COMM]));
		set_car_input(car_id, CAR_ACCEL, out[ACC_COMM] - out[DECC_COMM]);

		move_cars(T_STEP);
		get_car_position(car_id, pos);

		printf("\nstep %3d; in [", j);

		for (int i = 0; i < 10; i++)
			printf(" %6.3lf", in[i]);

		printf("]; out [%6.3lf %6.3lf]; pos [%6.3lf %6.3lf]\n\n\n",
				STEER * (out[LEFT_COMM] - out[RIGHT_COMM]), out[ACC_COMM] - out[DECC_COMM],
				pos[0], pos[1]);
		if (car_crashed(car_id)) break;
	}

	clear_cars();
	car_id = init_car(UNIT);
}

void print_arch() {
	for (int i = 0; i < net->n_lay; i++) {
		printf("%d\n", net->arch[i]);
	}
}
*/
int main(int argc, char **argv) {
	double min[2], max[2];
	if (argc < 4) {
		fprintf(stderr, "Usage: %s single_best_file multiple_best_file track_file\n", argv[0]);
		return ERROR;
	}

	srand(time(NULL));

	sing_net = (struct neural_net*) malloc(2 * sizeof(struct neural_net));
	mult_net  = sing_net + 1;
	if ((sing_weights = load_net_weights(argv[1], sing_net)) == NULL)
		return ERROR;
	if ((mult_weights = load_net_weights(argv[2], mult_net)) == NULL)
		return ERROR;

	if ((track_id = load_track(argv[3])) == -1) return ERROR;
	sing_car = init_car(track_id, UNIT);
	mult_car = init_car(track_id, UNIT);
	get_track_bound_box(track_id, min, max);

//	print_neur("show_neur.txt");
//	run_neur();
//	print_arch();

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE);
	glutInitWindowSize(max[0] + min[0], max[1] + min[1]);
	glutCreateWindow("multiple car test");

	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutTimerFunc(MILLIS, animate, 0);
	clock_gettime(CLOCK_MONOTONIC_RAW, &glob_start);
	glutMainLoop();

	return 0;
}
