#include <stdio.h>
#include <math.h>
#include <float.h>
#include <time.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "../../evol/evol_alg.h"
#include "../../neur/neur_net.h"
#include "../../model/model.h"

#define LEFT 0
#define RIGHT 1
#define ACCEL 2
#define DECCEL 3

#define N_STEPS 1000
#define T_STEP 0.02
#define UNIT 50.0

#define MAX_STEER 1.5

#define IN_S 10
#define OUT_S 4
#define LAY_N 4

#define GEN_M 0.0
#define GEN_D 1.0
#define MUT_P 0.1
#define MUT_M 0.0
#define MUT_D 0.1
#define POP 50
#define N_GEN 100000
#define K_TOURN 3

#define ERROR 1
#define TRCK_N 1

char track_names[TRCK_N][30] = {"track_long.txt"};//{"track.txt", "track_round.txt", "track_long.txt"};
int track_ids[TRCK_N] = {0};

int mult_arch[LAY_N] = {18, 25, 10, OUT_S};
struct neural_net *sing, *mult;
double angles[8] = {0, 0.25 * M_PI, 0.5 * M_PI, 0.75 * M_PI, M_PI, 1.25 * M_PI, 1.5 * M_PI, 1.75 * M_PI};
double *sing_weights;

double fitness(int size, double *mult_weights) {
	double out[OUT_S], in[18], fit = 0;
	int sing_car, mult_car;

	for (int j = 0; j < TRCK_N; j++) {
		for (int k = 0; k < 2; k++) {
			clear_cars(track_ids[j]);
			if (k == 0) {
				sing_car = init_car(track_ids[j], UNIT);
				mult_car = init_car(track_ids[j], UNIT);
			} else {
				mult_car = init_car(track_ids[j], UNIT);
				sing_car = init_car(track_ids[j], UNIT);
			}

			for (int i = 0; i < N_STEPS; i++) {
				in[0] = get_car_velocity(track_ids[j], sing_car);
				in[1] = next_chckpt_ang(track_ids[j], sing_car);
				for (int i = 0; i < 8; i++)
					in[i + 2] = get_wall_dist(track_ids[j], sing_car, angles[i]);

				eval_neur_net(sing, sing_weights, in, out);
				set_car_input(track_ids[j], sing_car, CAR_STEER, MAX_STEER * (out[LEFT] - out[RIGHT]));
				set_car_input(track_ids[j], sing_car, CAR_ACCEL, out[ACCEL] - out[DECCEL]);


				in[0] = get_car_velocity(track_ids[j], mult_car);
				in[1] = next_chckpt_ang(track_ids[j], mult_car);
				for (int i = 0; i < 8; i++) {
					in[i + 2] = get_wall_dist(track_ids[j], mult_car, angles[i]);
					in[i + 10] = get_car_dist(track_ids[j], mult_car, angles[i]);
				}

				eval_neur_net(mult, mult_weights, in, out);
				set_car_input(track_ids[j], mult_car, CAR_STEER, MAX_STEER * (out[LEFT] - out[RIGHT]));
				set_car_input(track_ids[j], mult_car, CAR_ACCEL, out[ACCEL] - out[DECCEL]);


				move_cars(track_ids[j], T_STEP);

				if (car_crashed(track_ids[j], mult_car)) break;
			}

			fit += get_track_completion(track_ids[j], mult_car);
		}
	}

	return fit / TRCK_N / 2;
}

struct individual *best_unit(struct individual *pop) {
	int best_i;
	double best_f = -DBL_MAX, fit;

	for (int i = 0; i < POP; i++) {
		fit = fitness(pop[i].size, pop[i].weights);
		if (fit > best_f) {
			best_i = i;
			best_f = fit;
		}
	}

	return pop + best_i;
}

void ev_log(int gen, double best) {
	if (gen % 1000 != 0) return;
	printf("gen: %d/%d, fit: %lf\r", gen, N_GEN, best);
}

int main(int argc, char **argv) {
	if (argc < 3) {
		fprintf(stderr, "Usage: %s pretrained_file result_file\n", argv[0]);
		return ERROR;
	}

	mult = new_network(LAY_N, mult_arch, sigmoid);
	sing = (struct neural_net*) malloc(sizeof(struct neural_net));
	if ((sing_weights = load_net_weights(argv[1], sing)) == NULL)
		return ERROR;


	srand(time(NULL));
	struct individual *pop = new_arch_pop_from_pretrained(POP, weights_size(sing), sing_weights,
			weights_size(mult), GEN_M, GEN_D, sing->n_lay, sing->arch, mult->n_lay, mult->arch);
	struct evolution *alg = new_evolution(K_TOURN, fitness, MUT_M, MUT_D, MUT_P);

	for (int i = 0; i < TRCK_N; i++)
		if ((track_ids[i] = load_track(track_names[i])) == -1) {
			fprintf(stderr, "Cannot load %s\n", track_names[i]);
			return ERROR;
		}

	learn_evol(alg, N_GEN, POP, pop, ev_log);
	printf("\n");
	struct individual *best = best_unit(pop);

	return write_net_weights(argv[2], mult, best->weights);
}
