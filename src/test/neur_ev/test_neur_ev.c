#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "../../evol/evol_alg.h"
#include "../../neur/neur_net.h"

#define DATA "dataset.txt"
#define DATA_S 64
#define IN_S 2
#define OUT_S 3

#define LAY_N 3

#define GEN_M 0.0
#define GEN_D 1.0
#define MUT_P 0.08
#define MUT_M 0.0
#define MUT_D 0.01
#define POP 50
#define N_GEN 10000000
#define K_TOURN 3

#define ERROR 1
#define RESULT "result.txt"

#define LAMBDA 0.01
#define BATCH 25

struct data_point {
	double input[IN_S];
	double output[OUT_S];
};

int architecture[LAY_N] = {IN_S, 8, OUT_S};
struct data_point data[DATA_S];
struct neural_net *net;

int load_data() {
	FILE *file;

	if ((file = fopen(DATA, "r")) == 0) {
		fprintf(stderr, "Cannot open file %s: %s\n", DATA, strerror(errno));
		return ERROR;
	}

	for (int i = 0; i < DATA_S; i++) {
		for (int j = 0; j < IN_S; j++)
			fscanf(file, "%lf", data[i].input + j);
		for (int j = 0; j < OUT_S; j++)
			fscanf(file, "%lf", data[i].output + j);
	}

	fclose(file);
	return 0;
}

double fitness(int size, double *weights) {
	double mse = 0.0, out[OUT_S];

	for (int i = 0; i < DATA_S; i++) {
		eval_neur_net(net, weights, data[i].input, out);
		for (int j = 0; j < OUT_S; j++)
			mse += pow(data[i].output[j] - out[j], 2);
	}

	mse /= (double) DATA_S;

//	for (int i = 0; i < size; i++)
//		mse -= LAMBDA * pow(weights[i], 2);

	return 1 / (1 + mse);
}

double batch_fitness(int size, double *weights) {
	double mse = 0.0, out[OUT_S];
	int index;

	for (int i = 0; i < BATCH; i++) {
		index = rand() % DATA_S;
		eval_neur_net(net, weights, data[index].input, out);
		for (int j = 0; j < OUT_S; j++)
			mse += pow(data[index].output[j] - out[j], 2);
	}

	mse /= (double) BATCH;

	return 1 / (1 + mse);
}

struct individual *best_unit(struct individual *pop) {
	int best_i;
	double best_f = 100000.0, fit;

	for (int i = 0; i < POP; i++) {
		fit = fitness(pop[i].size, pop[i].weights);
		if (fit < best_f) {
			best_i = i;
			best_f = fit;
		}
	}

	return pop + best_i;
}

int write_result(double *weights) {
	FILE *file;
	if ((file = fopen(RESULT, "w")) == 0) {
		fprintf(stderr, "Cannot open file %s: %s\n", RESULT, strerror(errno));
		return ERROR;
	}

	double out[OUT_S];
	for (int i = 0; i < DATA_S; i++) {
		eval_neur_net(net, weights, data[i].input, out);
		for (int j = 0; j < IN_S; j++)
			fprintf(file, " %.04lf", data[i].input[j]);
		for (int j = 0; j < OUT_S; j++)
			fprintf(file, " %.04lf(%.01lf)", out[j], data[i].output[j]);
		fprintf(file, "\n");
	}

	fclose(file);

	return 0;
}

void write_best(struct individual *best) {
	int n = weights_size(net);

	printf("architecture: ");
	for (int i = 0; i < LAY_N; i++) {
		printf("%d", architecture[i]);
		if (i == LAY_N - 1) continue;
		printf("x");
	}

	printf("\nweights:");
	for (int i = 0; i < n; i++)
		printf(" %.04lf", best->weights[i]);

	printf("\nFitness [0,1]: %lf\n", fitness(best->size, best->weights));
}

void test_log(int gen, double best) {
	if (gen % 2500 != 0) return;
	printf("gen: %d/%d, best: %lf\r", gen, N_GEN, best);
}

int main(int argc, char **argv) {
	if (load_data() == ERROR) return ERROR;
	net = new_network(LAY_N, architecture, sigmoid);
	srand(time(NULL));
	struct individual *pop = random_pop(POP, weights_size(net), GEN_M, GEN_D);
	struct evolution *alg = new_evolution(K_TOURN, batch_fitness, MUT_M, MUT_D, MUT_P);

	learn_evol(alg, N_GEN, POP, pop, test_log);
	struct individual *best = best_unit(pop);

	write_best(best);
	return write_result(best->weights);
}
