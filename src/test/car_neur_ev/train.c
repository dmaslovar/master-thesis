#include <stdio.h>
#include <math.h>
#include <float.h>
#include <time.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "../../evol/evol_alg.h"
#include "../../neur/neur_net.h"
#include "../../model/model.h"

#define LEFT 0
#define RIGHT 1
#define ACCEL 2
#define DECCEL 3

#define N_STEPS 1000
#define T_STEP 0.02
#define UNIT 50.0

#define MAX_STEER 1.5

#define IN_S 10
#define OUT_S 4
#define LAY_N 4

#define GEN_M 0.0
#define GEN_D 1.0
#define MUT_P 0.1
#define MUT_M 0.0
#define MUT_D 0.1
#define POP 50
#define N_GEN 300000
#define K_TOURN 3

#define ERROR 1
#define TRCK_N 1

char track_names[TRCK_N][30] = {"track_long.txt"};//{"track.txt", "track_round.txt", "track_long.txt"};
int track_ids[TRCK_N] = {0};

int architecture[LAY_N] = {IN_S, 15, 8, OUT_S};
struct neural_net *net;
double angles[8] = {0, 0.25 * M_PI, 0.5 * M_PI, 0.75 * M_PI, M_PI, 1.25 * M_PI, 1.5 * M_PI, 1.75 * M_PI};

double fitness(int size, double *weights) {
	double out[OUT_S], in[IN_S], fit = 0;
	int car_id;

	for (int j = 0; j < TRCK_N; j++) {
		clear_cars(track_ids[j]);
		car_id = init_car(track_ids[j], UNIT);
		for (int i = 0; i < N_STEPS; i++) {
			in[0] = get_car_velocity(track_ids[j], car_id);
			in[1] = next_chckpt_ang(track_ids[j], car_id);
			for (int i = 0; i < 8; i++)
				in[i + 2] = get_wall_dist(track_ids[j], car_id, angles[i]);

			eval_neur_net(net, weights, in, out);
			set_car_input(track_ids[j], car_id, CAR_STEER, MAX_STEER * (out[LEFT] - out[RIGHT]));
			set_car_input(track_ids[j], car_id, CAR_ACCEL, out[ACCEL] - out[DECCEL]);
			move_cars(track_ids[j], T_STEP);

			if (car_crashed(track_ids[j], car_id)) break;
		}

		fit += get_track_completion(track_ids[j], car_id);
	}

	return fit / TRCK_N;
}
/*
double fitness_print(int size, double *weights) {
	FILE *fd;
	double out[OUT_S], in[10], end[2], compl;
	int car_id;

	if ((fd = fopen("steps.txt", "w")) == 0) {
		fprintf(stderr, "Cannot open steps file: %s\n", strerror(errno));
		return ERROR;
	}

	clear_cars();
	car_id = init_car(UNIT);

	for (int i = 0; i < N_STEPS; i++) {
		in[0] = get_car_velocity(car_id);
		in[1] = next_chckpt_ang(car_id);
		for (int i = 0; i < 8; i++)
			in[i + 2] = get_wall_dist(car_id, angles[i]);

		eval_neur_net(net, weights, in, out);
		set_car_input(car_id, CAR_STEER, MAX_STEER * (out[LEFT] - out[RIGHT]));
		set_car_input(car_id, CAR_ACCEL, out[ACCEL] - out[DECCEL]);
		move_cars(T_STEP);

		get_car_position(car_id, end);
		printf("\nstep %3d; in [", i);

		for (int i = 0; i < IN_S; i++)
			printf(" %6.3lf", in[i]);
		printf("]; out [%6.3lf %6.3lf]; pos [%6.3lf %6.3lf]\n\n\n",
					MAX_STEER * (out[LEFT] - out[RIGHT]),
					out[ACCEL] - out[DECCEL],
					end[0], end[1]);

		if (car_crashed(car_id)) break;
	}

	fclose(fd);

	get_car_position(car_id, end);
	compl = get_track_completion(car_id);

	printf("car finishes at [%.3lf %.3lf] with %.3lf completion\n", end[0], end[1], compl);

	return compl;
}
*/
struct individual *best_unit(struct individual *pop) {
	int best_i;
	double best_f = -DBL_MAX, fit;

	for (int i = 0; i < POP; i++) {
		fit = fitness(pop[i].size, pop[i].weights);
		if (fit > best_f) {
			best_i = i;
			best_f = fit;
		}
	}

	return pop + best_i;
}

void ev_log(int gen, double best) {
	if (gen % 1000 != 0) return;
	printf("gen: %d/%d, fit: %lf\r", gen, N_GEN, best);
}

int main(int argc, char **argv) {
	if (argc < 2) {
		fprintf(stderr, "Missing result_file argument\nUsage: %s result_file\n", argv[0]);
		return ERROR;
	}

	net = new_network(LAY_N, architecture, sigmoid);
	srand(time(NULL));
	struct individual *pop = random_pop(POP, weights_size(net), GEN_M, GEN_D);
	struct evolution *alg = new_evolution(K_TOURN, fitness, MUT_M, MUT_D, MUT_P);

	for (int i = 0; i < TRCK_N; i++)
		if ((track_ids[i] = load_track(track_names[i])) == -1) {
			fprintf(stderr, "Cannot load %s\n", track_names[i]);
			return ERROR;
		}

	learn_evol(alg, N_GEN, POP, pop, ev_log);
	printf("\n");
	struct individual *best = best_unit(pop);

//	fitness_print(best->size, best->weights);

	return write_net_weights(argv[1], net, best->weights);
}
