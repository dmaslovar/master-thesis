#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <float.h>
#include <config.h>
#include <neur_net.h>
#include <evol_alg.h>
#include <model.h>
#include <fitness.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <math.h>

struct settings_instance {
	int sens;
	char *hid;
	double dev;
	double prob;
	double alpha;
	double beta;
	double fit;
};

struct _global_variables {
	int *n_wall_s;
	double *alphas, *betas, *devs, *probs;
	char **act_names;
	double ******values;
	int n_neur, n_hid, n_dev, n_prob, n_alpha, n_beta;
} glob;

int init_lengths(struct configuration *config) {
	if (get_config_integer(config, "number.of.networks", &(glob.n_neur)) == CONF_ERR) return 1;
	if (get_config_integer(config, "param.alpha.number", &(glob.n_alpha)) == CONF_ERR) return 1;
	if (get_config_integer(config, "param.beta.number", &(glob.n_beta)) == CONF_ERR) return 1;
	if (get_config_integer(config, "param.hidden.activation.number", &(glob.n_hid)) == CONF_ERR) return 1;
	if (get_config_integer(config, "param.mutation.deviation.number", &(glob.n_dev)) == CONF_ERR) return 1;
	if (get_config_integer(config, "param.mutation.probability.number", &(glob.n_prob)) == CONF_ERR) return 1;
	return 0;
}

int init_sens(struct configuration *config) {
	glob.n_wall_s = (int*) malloc(glob.n_neur * sizeof(int));
	char phase_buf[30] = {0};
	struct configuration *sub_config;

	for (int i = 0; i < glob.n_neur; i++) {
		snprintf(phase_buf, 30, "network%d", i + 1);
		sub_config = get_sub_config(config, phase_buf);
		if (get_config_integer(sub_config, "wall.sensors", glob.n_wall_s + i) == CONF_ERR) return 1;
	}

	return 0;
}

int init_arrays(struct configuration *config) {
	if (get_config_double_array(config, "param.alpha.values", glob.n_alpha, &(glob.alphas)) == CONF_ERR) return 1;
	if (get_config_double_array(config, "param.beta.values", glob.n_beta, &(glob.betas)) == CONF_ERR) return 1;
	if (get_config_string_array(config, "param.hidden.activation.values", glob.n_hid, &(glob.act_names)) == CONF_ERR) return 1;
	if (get_config_double_array(config, "param.mutation.deviation.values", glob.n_dev, &(glob.devs)) == CONF_ERR) return 1;
	if (get_config_double_array(config, "param.mutation.probability.values", glob.n_prob, &(glob.probs)) == CONF_ERR) return 1;
	if (init_sens(config) == 1) return 1;

	glob.values = (double******) malloc(glob.n_neur * sizeof(double*****));
	for (int n_i = 0; n_i < glob.n_neur; n_i++) {
		glob.values[n_i] = (double*****) malloc(glob.n_hid * sizeof(double****));
		for (int h_i = 0; h_i < glob.n_hid; h_i++) {
			glob.values[n_i][h_i] = (double****) malloc(glob.n_dev * sizeof(double***));
			for (int d_i = 0; d_i < glob.n_dev; d_i++) {
				glob.values[n_i][h_i][d_i] = (double***) malloc(glob.n_prob * sizeof(double**));
				for (int p_i = 0; p_i < glob.n_prob; p_i++) {
					glob.values[n_i][h_i][d_i][p_i] = (double**) malloc(glob.n_alpha * sizeof(double*));
					for (int a_i = 0; a_i < glob.n_alpha; a_i++) {
						glob.values[n_i][h_i][d_i][p_i][a_i] = (double*) malloc(glob.n_beta * sizeof(double));
						for (int b_i = 0; b_i < glob.n_beta; b_i++)
							glob.values[n_i][h_i][d_i][p_i][a_i][b_i] = -DBL_MAX;
					}
				}
			}
		}
	}
	return 0;
}

int fill_settings_instance(struct settings_instance *inst, struct configuration *config) {
	if (get_config_integer(config, "wall.sensors", &(inst->sens)) == CONF_ERR) return 1;
	if (inst->hid != NULL) free(inst->hid);
	if (get_config_string(config, "phase1.architecture.hidden.activation", &(inst->hid)) == CONF_ERR) return 1;
	if (get_config_double(config, "mutation.deviation", &(inst->dev)) == CONF_ERR) return 1;
	if (get_config_double(config, "mutation.probability", &(inst->prob)) == CONF_ERR) return 1;
	if (get_config_double(config, "alpha", &(inst->alpha)) == CONF_ERR) return 1;
	if (get_config_double(config, "beta", &(inst->beta)) == CONF_ERR) return 1;
	if (get_config_double(config, "winner.fitness", &(inst->fit)) == CONF_ERR) return 1;
//	printf("loaded sens=%d hid=%s dev=%lf prob=%lf alpha=%lf beta=%lf\n",
//			inst->sens, inst->hid, inst->dev, inst->prob, inst->alpha, inst->beta);
	return 0;
}

int load_result_file(char *filename, struct settings_instance *inst) {
	int n_i, h_i, d_i, p_i, a_i, b_i;
	struct configuration *res_config;

	if ((res_config = config_file(filename)) == NULL) {
		fprintf(stderr, "Cannot read configuration %s\n", filename);
		return 1;
	}
	if (fill_settings_instance(inst, res_config) == 1) {
		fprintf(stderr, "Cannot load %s, skipping...\n", filename);
		free_config(res_config);
		return 1;
	}
	free_config(res_config);

	for (n_i = 0; n_i < glob.n_neur && glob.n_wall_s[n_i] != inst->sens; n_i++);
	if (n_i == glob.n_neur) {
		fprintf(stderr, "Wall sensor value not found: %d, skipping...\n", inst->sens);
		return 1;
	}
	for (h_i = 0; h_i < glob.n_hid && strcmp(glob.act_names[h_i], inst->hid); h_i++);
	if (h_i == glob.n_hid) {
		fprintf(stderr, "Hidden activation value not found: %s, skipping...\n", inst->hid);
		return 1;
	}
	for (d_i = 0; d_i < glob.n_dev && glob.devs[d_i] != inst->dev; d_i++);
	if (d_i == glob.n_dev) {
		fprintf(stderr, "Deviation value not found: %lf, skipping...\n", inst->dev);
		return 1;
	}
	for (p_i = 0; p_i < glob.n_prob && glob.probs[p_i] != inst->prob; p_i++);
	if (p_i == glob.n_prob) {
		fprintf(stderr, "Mutation probability value not found: %lf, skipping...\n", inst->prob);
		return 1;
	}
	for (a_i = 0; a_i < glob.n_alpha && glob.alphas[a_i] != inst->alpha; a_i++);
	if (a_i == glob.n_alpha) {
		fprintf(stderr, "Alpha value not found: %lf, skipping...\n", inst->alpha);
		return 1;
	}
	for (b_i = 0; b_i < glob.n_beta && glob.betas[b_i] != inst->beta; b_i++);
	if (b_i == glob.n_beta) {
		fprintf(stderr, "Beta value not found: %lf, skipping...\n", inst->beta);
		return 1;
	}
	glob.values[n_i][h_i][d_i][p_i][a_i][b_i] = inst->fit;
	return 0;
}

void print_sens(FILE *f) {
	for (int i = 0; i < glob.n_neur; i++) {
		fprintf(f, "%d", glob.n_wall_s[i]);
		if (i == glob.n_neur - 1)
			fprintf(f, "\n");
		else
			fprintf(f, ";");
	}

	for (int h_i = 0; h_i < glob.n_hid; h_i++)
		for (int d_i = 0; d_i < glob.n_dev; d_i++)
			for (int p_i = 0; p_i < glob.n_prob; p_i++)
				for (int a_i = 0; a_i < glob.n_alpha; a_i++)
					for (int b_i = 0; b_i < glob.n_beta; b_i++) {
						for (int n_i = 0; n_i < glob.n_neur; n_i++) {
							if (glob.values[n_i][h_i][d_i][p_i][a_i][b_i] != -DBL_MAX)
								fprintf(f, "%lf", glob.values[n_i][h_i][d_i][p_i][a_i][b_i]);
							if (n_i == glob.n_neur - 1)
								fprintf(f, "\n");
							else
								fprintf(f, ";");
						}
					}
}

void print_hid(FILE *f) {
	for (int i = 0; i < glob.n_hid; i++) {
		fprintf(f, "%s", glob.act_names[i]);
		if (i == glob.n_hid - 1)
			fprintf(f, "\n");
		else
			fprintf(f, ";");
	}

	for (int n_i = 0; n_i < glob.n_neur; n_i++)
		for (int d_i = 0; d_i < glob.n_dev; d_i++)
			for (int p_i = 0; p_i < glob.n_prob; p_i++)
				for (int a_i = 0; a_i < glob.n_alpha; a_i++)
					for (int b_i = 0; b_i < glob.n_beta; b_i++) {
						for (int h_i = 0; h_i < glob.n_hid; h_i++) {
							if (glob.values[n_i][h_i][d_i][p_i][a_i][b_i] != -DBL_MAX)
								fprintf(f, "%lf", glob.values[n_i][h_i][d_i][p_i][a_i][b_i]);
							if (h_i == glob.n_hid - 1)
								fprintf(f, "\n");
							else
								fprintf(f, ";");
						}
					}
}

void print_dev(FILE *f) {
	for (int i = 0; i < glob.n_dev; i++) {
		fprintf(f, "%lf", glob.devs[i]);
		if (i == glob.n_dev - 1)
			fprintf(f, "\n");
		else
			fprintf(f, ";");
	}

	for (int n_i = 0; n_i < glob.n_neur; n_i++)
		for (int h_i = 0; h_i < glob.n_hid; h_i++)
			for (int p_i = 0; p_i < glob.n_prob; p_i++)
				for (int a_i = 0; a_i < glob.n_alpha; a_i++)
					for (int b_i = 0; b_i < glob.n_beta; b_i++) {
						for (int d_i = 0; d_i < glob.n_dev; d_i++) {
							if (glob.values[n_i][h_i][d_i][p_i][a_i][b_i] != -DBL_MAX)
								fprintf(f, "%lf", glob.values[n_i][h_i][d_i][p_i][a_i][b_i]);
							if (d_i == glob.n_dev - 1)
								fprintf(f, "\n");
							else
								fprintf(f, ";");
						}
					}
}

void print_prob(FILE *f) {
	for (int i = 0; i < glob.n_prob; i++) {
		fprintf(f, "%lf", glob.probs[i]);
		if (i == glob.n_prob - 1)
			fprintf(f, "\n");
		else
			fprintf(f, ";");
	}

	for (int n_i = 0; n_i < glob.n_neur; n_i++)
		for (int h_i = 0; h_i < glob.n_hid; h_i++)
			for (int d_i = 0; d_i < glob.n_dev; d_i++)
				for (int a_i = 0; a_i < glob.n_alpha; a_i++)
					for (int b_i = 0; b_i < glob.n_beta; b_i++) {
						for (int p_i = 0; p_i < glob.n_prob; p_i++) {
							if (glob.values[n_i][h_i][d_i][p_i][a_i][b_i] != -DBL_MAX)
								fprintf(f, "%lf", glob.values[n_i][h_i][d_i][p_i][a_i][b_i]);
							if (p_i == glob.n_prob - 1)
								fprintf(f, "\n");
							else
								fprintf(f, ";");
						}
					}
}

void print_alpha(FILE *f) {
	for (int i = 0; i < glob.n_alpha; i++) {
		fprintf(f, "%lf", glob.alphas[i]);
		if (i == glob.n_alpha - 1)
			fprintf(f, "\n");
		else
			fprintf(f, ";");
	}

	for (int n_i = 0; n_i < glob.n_neur; n_i++)
		for (int h_i = 0; h_i < glob.n_hid; h_i++)
			for (int d_i = 0; d_i < glob.n_dev; d_i++)
				for (int p_i = 0; p_i < glob.n_prob; p_i++)
					for (int b_i = 0; b_i < glob.n_beta; b_i++) {
						for (int a_i = 0; a_i < glob.n_alpha; a_i++) {
							if (glob.values[n_i][h_i][d_i][p_i][a_i][b_i] != -DBL_MAX)
								fprintf(f, "%lf", glob.values[n_i][h_i][d_i][p_i][a_i][b_i]);
							if (a_i == glob.n_alpha - 1)
								fprintf(f, "\n");
							else
								fprintf(f, ";");
						}
					}
}

void print_beta(FILE *f) {
	for (int i = 0; i < glob.n_beta; i++) {
		fprintf(f, "%lf", glob.betas[i]);
		if (i == glob.n_beta - 1)
			fprintf(f, "\n");
		else
			fprintf(f, ";");
	}

	for (int n_i = 0; n_i < glob.n_neur; n_i++)
		for (int h_i = 0; h_i < glob.n_hid; h_i++)
			for (int d_i = 0; d_i < glob.n_dev; d_i++)
				for (int p_i = 0; p_i < glob.n_prob; p_i++)
					for (int a_i = 0; a_i < glob.n_alpha; a_i++) {
						for (int b_i = 0; b_i < glob.n_beta; b_i++) {
							if (glob.values[n_i][h_i][d_i][p_i][a_i][b_i] != -DBL_MAX)
								fprintf(f, "%lf", glob.values[n_i][h_i][d_i][p_i][a_i][b_i]);
							if (b_i == glob.n_beta - 1)
								fprintf(f, "\n");
							else
								fprintf(f, ";");
						}
					}
}

void print_result(char *dir, char *filename, void (*fun)(FILE*)) {
	char res_file[60];
	FILE *f;

	snprintf(res_file, 60, "%s/%s", dir, filename);
	if ((f = fopen(res_file, "w")) == NULL) {
		fprintf(stderr, "Cannot open file %s: %s\n", res_file, strerror(errno));
		return;
	}
	fun(f);
	fclose(f);
}

int main(int argc, char **argv) {
	struct configuration *config;
	struct settings_instance inst;
	inst.hid = NULL;
	int dir_len;
	char dir[50] = {0};
	struct stat dir_stat = {0};
	int n_files = argc - 3;
	int n_skipped = 0;

	if (argc < 4) {
		fprintf(stderr, "Usage: %s conf_file output_dir files...\n", argv[0]);
		return 1;
	}

	printf("Loading config...");

	if ((config = config_file(argv[1])) == NULL)
		return 1;

	if (init_lengths(config) == 1) return 1;
	if (init_arrays(config) == 1) return 1;

	printf(" done!\nLoading files...");

	for (int i = 3; i < argc; i++) {
		if (load_result_file(argv[i], &inst) == 1)
			n_skipped++;
	}
	printf("done\n");
	if (n_skipped != 0)
		printf("Skipped %d/%d files\n", n_skipped, n_files);

	dir_len = strlen(argv[2]);
	memcpy(dir, argv[2], dir_len);
	if (dir[dir_len - 1] == '/')
		dir[dir_len - 1] = '\0';
	if (stat(dir, &dir_stat) == -1)
		if (mkdir(dir, 0777) == -1) {
			fprintf(stderr, "Cannot create result dir: %s\n", strerror(errno));
			return 1;
		}

	print_result(dir, "sensors.txt", print_sens);
	print_result(dir, "hidden_activations.txt", print_hid);
	print_result(dir, "mutation_deviations.txt", print_dev);
	print_result(dir, "mutation_probabilities.txt", print_prob);
	print_result(dir, "alphas.txt", print_alpha);
	print_result(dir, "betas.txt", print_beta);
/*	snprintf(res_file, 60, "%s/sensors.txt", dir);
	if ((f = fopen(res_file, "w")) == NULL) {
		fprintf(stderr, "Cannot open file %s: %s\n", res_file, strerror(errno));
		return 1;
	}
	print_sens(f);
	fclose(f);

	snprintf(res_file, 60, "%s/hidden_activations.txt", dir);
	if ((f = fopen(res_file, "w")) == NULL) {
		fprintf(stderr, "Cannot open file %s: %s\n", res_file, strerror(errno));
		return 1;
	}
	print_hid(f);
	fclose(f);

	snprintf(res_file, 60, "%s/mutation_deviations.txt", dir);
	if ((f = fopen(res_file, "w")) == NULL) {
		fprintf(stderr, "Cannot open file %s: %s\n", res_file, strerror(errno));
		return 1;
	}
	print_dev(f);
	fclose(f);

	snprintf(res_file, 60, "%s/mutation_probabilities.txt", dir);
	if ((f = fopen(res_file, "w")) == NULL) {
		fprintf(stderr, "Cannot open file %s: %s\n", res_file, strerror(errno));
		return 1;
	}
	print_prob(f);
	fclose(f);

	snprintf(res_file, 60, "%s/sensors.txt", dir);
	if ((f = fopen(res_file, "w")) == NULL) {
		fprintf(stderr, "Cannot open file %s: %s\n", res_file, strerror(errno));
		return 1;
	}
	print_sens(f);
	fclose(f);

	snprintf(res_file, 60, "%s/sensors.txt", dir);
	if ((f = fopen(res_file, "w")) == NULL) {
		fprintf(stderr, "Cannot open file %s: %s\n", res_file, strerror(errno));
		return 1;
	}
	print_sens(f);
	fclose(f);*/


	return 0;
}
