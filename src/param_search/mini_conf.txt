# dynamic params
number.of.networks = 1

network1.phase1.length = 4
network1.phase1.architecture = 8 15 10 2
network1.phase2.length = 4
network1.phase2.architecture = 14 20 12 2
network1.wall.sensors = 6
network1.car.sensors = 6

param.alpha.number = 2
param.alpha.values = 0.3 0.5
param.beta.number = 1
param.beta.values = 0.3
param.hidden.activation.number = 2
param.hidden.activation.values = tanh relu
param.mutation.deviation.number = 1
param.mutation.deviation.values = 0.05
param.mutation.probability.number = 1
param.mutation.probability.values = 0.01

#static params
k.tournament = 3
population.size = 50
population.mean = 0
population.deviation = 1
mutation.mean = 0
phase1.generations = 1000
phase2.generations = 1000
phase3.generations = 1000
number.of.steps = 500

# tracks
train.number.of.tracks = 1
train.track.files = ../test/car_neur_ev/track_round.txt
train.number.of.cars = 2
train.car.unit = 50
test.number.of.tracks = 1
test.track.files = ../test/car_neur_ev/track_round.txt
test.number.of.cars = 2
test.car.unit = 50
