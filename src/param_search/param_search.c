#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <float.h>
#include <config.h>
#include <neur_net.h>
#include <evol_alg.h>
#include <model.h>
#include <fitness.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <math.h>

#define CONFIG "conf.txt"
#define RESULT "result.txt"

struct _static_settings {
	int k;
	int pop_size;
	double pop_mean;
	double pop_dev;
	double mut_mean;
	long p1_gens;
	long p2_gens;
	long p3_gens;
	int steps;
	int ids_len;
	struct track_ids *ids;
} settings;

char run_dir[70];
FILE *log_gen_file;

typedef double (*act_fun)(double);

void log_gens(int gen, double best) {
	if (gen % 500 != 0) return;
	fprintf(log_gen_file, "gen: %d best: %lf\n", gen, best);
}

double *preserve_best(struct population *pop) {
	int best_i;
	double best = -DBL_MAX, *w;

	for (int i = 0; i < pop->size; i++)
		if (pop->pop[i].fit > best) {
			best = pop->pop[i].fit;
			best_i = i;
		}

	w = (double*) malloc(pop->pop[best_i].size * sizeof(double));
	memcpy(w, pop->pop[best_i].weights, pop->pop[best_i].size * sizeof(double));
	return w;
}

void write_results(FILE *f, int w_size, double *w, struct configuration *config) {
	char *buf;

	fprintf(f, "# results\nresult.size = %d\nresult.weights =", w_size);
	for (int i = 0; i < w_size; i++)
		fprintf(f, " %lf", w[i]);

	fprintf(f, "\n\n# neural network\n");
	get_config_value(config, "phase2.architecture.length", &buf);
	fprintf(f, "architecture.length = %s\n", buf);
	get_config_value(config, "phase2.architecture", &buf);
	fprintf(f, "architecture = %s\n", buf);
	get_config_value(config, "phase2.architecture.hidden.activation", &buf);
	fprintf(f, "architecture.hidden.activation = %s\n", buf);
	get_config_value(config, "phase2.architecture.output.activation", &buf);
	fprintf(f, "architecture.output.activation = %s\n", buf);

	fprintf(f, "\n# input settings\n");
	get_config_value(config, "number.of.wall.sensors", &buf);
	fprintf(f, "number.of.wall.sensors = %s\n", buf);
	get_config_value(config, "number.of.car.sensors", &buf);
	fprintf(f, "number.of.car.sensors = %s\n", buf);
}

double *learn_parameter_set(struct neural_net *p1_net, struct neural_net *p2_net, double alpha, double beta, double mut_dev, double mut_prob) {
	char log_file_name[80];
	double *p1_best, *p2_best, *p3_best;
	int p1_size, p2_size;
	struct evolution *evol;
	struct population *pop;

	p1_size = weights_size(p1_net);
	p2_size = weights_size(p2_net);

	snprintf(log_file_name, 80, "%sphase1_gens.txt", run_dir);
	if ((log_gen_file = fopen(log_file_name, "w")) == NULL) {
		fprintf(stderr, "Cannot open %s: %s\n", log_file_name, strerror(errno));
		return NULL;
	}
	pop = random_pop(settings.pop_size, p1_size, settings.pop_mean, settings.pop_dev);
	evol = new_evolution(settings.k, phase1_fit, settings.mut_mean, mut_dev, mut_prob, settings.p1_gens, pop);
	learn_evol(evol, log_gens);
	fclose(log_gen_file);

	p1_best = preserve_best(evol->pop);
	set_phase1_weights(p1_best);
	release_evolution(evol);

	snprintf(log_file_name, 80, "%sphase2_gens.txt", run_dir);
	if ((log_gen_file = fopen(log_file_name, "w")) == NULL) {
		fprintf(stderr, "Cannot open %s: %s\n", log_file_name, strerror(errno));
		return NULL;
	}
	pop = new_arch_pop_from_pretrained(settings.pop_size, p1_size, p1_best, p2_size, settings.pop_mean, settings.pop_dev,
										p1_net->n_lay, p1_net->arch, p2_net->n_lay, p2_net->arch);
	evol = new_evolution(settings.k, phase2_fit, settings.mut_mean, mut_dev, mut_prob, settings.p2_gens, pop);
	learn_evol(evol, log_gens);
	fclose(log_gen_file);

	p2_best = preserve_best(evol->pop);
	release_evolution(evol);

	snprintf(log_file_name, 80, "%sphase3_gens.txt", run_dir);
	if ((log_gen_file = fopen(log_file_name, "w")) == NULL) {
		fprintf(stderr, "Cannot open %s: %s\n", log_file_name, strerror(errno));
		return NULL;
	}
	pop = pop_from_pretrained(settings.pop_size, p2_size, p2_best, settings.pop_mean, settings.pop_dev);
	evol = new_evolution(settings.k, phase3_fit, settings.mut_mean, mut_dev, mut_prob, settings.p3_gens, pop);
	learn_evol(evol, log_gens);
	fclose(log_gen_file);

	p3_best = preserve_best(evol->pop);
	release_evolution(evol);
	free(p1_best);
	free(p2_best);

	return p3_best;
}

int init_activation_functions(int n, char **names, act_fun **hidden) {
	act_fun *hid = (act_fun*) malloc(n * sizeof(act_fun));

	for (int i = 0; i < n; i++) {
		if (strcmp(names[i], _NN_ACT_SIGM))
			hid[i] = sigmoid;
		else if (strcmp(names[i], _NN_ACT_TANH))
			hid[i] = tanh;
		else if (strcmp(names[i], _NN_ACT_RELU))
			hid[i] = relu;
		else {
			fprintf(stderr, "Unknown activation function: %s\n", names[i]);
			return 1;
		}
	}

	*hidden = hid;
	return 0;
}

int init_dynamic(struct configuration *config, int *n_neur, int *n_alpha, int *n_beta, int *n_hid, int *n_dev, int *n_prob) {
	if (get_config_integer(config, "number.of.networks", n_neur) == CONF_ERR) return 1;
	if (get_config_integer(config, "param.alpha.number", n_alpha) == CONF_ERR) return 1;
	if (get_config_integer(config, "param.beta.number", n_beta) == CONF_ERR) return 1;
	if (get_config_integer(config, "param.hidden.activation.number", n_hid) == CONF_ERR) return 1;
	if (get_config_integer(config, "param.mutation.deviation.number", n_dev) == CONF_ERR) return 1;
	if (get_config_integer(config, "param.mutation.probability.number", n_prob) == CONF_ERR) return 1;
	return 0;
}

int init_nets(struct configuration *config, int n_neur, struct neural_net ***phase1_nets, struct neural_net ***phase2_nets,
			int **wall_sensors, int **car_sensors) {
	char phase_buf[30] = {0};
	struct configuration *sub_config;
	int n_lay, *arch;

	struct neural_net **p1_nets = (struct neural_net**) malloc(n_neur * sizeof(struct neural_net*));
	struct neural_net **p2_nets = (struct neural_net**) malloc(n_neur * sizeof(struct neural_net*));
	int *wall_s = (int*) malloc(n_neur * sizeof(int));
	int *car_s = (int*) malloc(n_neur * sizeof(int));

	for (int i = 0; i < n_neur; i++) {
		snprintf(phase_buf, 30, "network%d", i + 1);
		sub_config = get_sub_config(config, phase_buf);

		if (get_config_integer(sub_config, "phase1.length", &n_lay) == CONF_ERR) return 1;
		if (get_config_integer_array(sub_config, "phase1.architecture", n_lay, &arch) == CONF_ERR) return 1;
		p1_nets[i] = new_network(n_lay, arch, NULL, tanh);

		if (get_config_integer(sub_config, "phase2.length", &n_lay) == CONF_ERR) return 1;
		if (get_config_integer_array(sub_config, "phase2.architecture", n_lay, &arch) == CONF_ERR) return 1;
		p2_nets[i] = new_network(n_lay, arch, NULL, tanh);

		if (get_config_integer(sub_config, "wall.sensors", wall_s + i) == CONF_ERR) return 1;
		if (get_config_integer(sub_config, "car.sensors", car_s + i) == CONF_ERR) return 1;
	}

	*phase1_nets = p1_nets;
	*phase2_nets = p2_nets;
	*wall_sensors = wall_s;
	*car_sensors = car_s;
	return 0;
}

int init_static(struct configuration *config) {
	struct configuration *train_tracks_conf = get_sub_config(config, "train");
	if (get_config_integer(config, "k.tournament", &(settings.k)) == CONF_ERR) return 1;
	if (get_config_integer(config, "population.size", &(settings.pop_size)) == CONF_ERR) return 1;
	if (get_config_double(config, "population.mean", &(settings.pop_mean)) == CONF_ERR) return 1;
	if (get_config_double(config, "population.deviation", &(settings.pop_dev)) == CONF_ERR) return 1;
	if (get_config_double(config, "mutation.mean", &(settings.mut_mean)) == CONF_ERR) return 1;
	if (get_config_long(config, "phase1.generations", &(settings.p1_gens)) == CONF_ERR) return 1;
	if (get_config_long(config, "phase2.generations", &(settings.p2_gens)) == CONF_ERR) return 1;
	if (get_config_long(config, "phase3.generations", &(settings.p3_gens)) == CONF_ERR) return 1;
	if (get_config_integer(config, "number.of.steps", &(settings.steps)) == CONF_ERR) return 1;
	if (conf_load_tracks(&(settings.ids), &(settings.ids_len), train_tracks_conf) < 0) return 1;

	return 0;
}

void write_neur_net(FILE *f, char *prefix, struct neural_net *net, char *hid_act) {
	fprintf(f, "%s.architecture.length = %d\n", prefix, net->n_lay);
	fprintf(f, "%s.architecture =", prefix);
	for (int i = 0; i < net->n_lay; i++)
		fprintf(f, " %d", net->arch[i]);
	fprintf(f, "\n%s.architecture.hidden.activation = %s\n", prefix, hid_act);
	fprintf(f, "%s.architecture.output.activation = tanh\n", prefix);
}

void write_result(FILE *f, struct configuration *config, struct neural_net *p1_net, struct neural_net *p2_net, int n_wall_s,
					int n_car_s, char *act_name, double dev, double prob, double alpha, double beta, double *win_w, double win_fit) {
	char *buf;
	int len = weights_size(p2_net);

	fprintf(f, "# architectures\n");
	write_neur_net(f, "phase1", p1_net, act_name);
	write_neur_net(f, "phase2", p2_net, act_name);
	fprintf(f, "wall.sensors = %d\n", n_wall_s);
	fprintf(f, "car.sensors = %d\n", n_car_s);

	fprintf(f, "\n# other params\n");
	fprintf(f, "mutation.deviation = %lf\n", dev);
	fprintf(f, "mutation.probability = %lf\n", prob);
	fprintf(f, "alpha = %lf\n", alpha);
	fprintf(f, "beta = %lf\n", beta);

	fprintf(f, "\n# static params\n");
	fprintf(f, "k.tournament = %d\n", settings.k);
	fprintf(f, "population.size = %d\n", settings.pop_size);
	fprintf(f, "population.mean = %lf\n", settings.pop_mean);
	fprintf(f, "population.deviation = %lf\n", settings.pop_dev);
	fprintf(f, "mutation.mean = %lf\n", settings.mut_mean);
	fprintf(f, "phase1.generations = %ld\n", settings.p1_gens);
	fprintf(f, "phase2.generations = %ld\n", settings.p2_gens);
	fprintf(f, "phase3.generations = %ld\n", settings.p3_gens);
	fprintf(f, "number.of.steps = %d\n", settings.steps);

	fprintf(f, "\n# tracks params\n");
	get_config_value(config, "train.number.of.tracks", &buf);
	fprintf(f, "train.number.of.tracks = %s\n", buf);
	get_config_value(config, "train.track.files", &buf);
	fprintf(f, "train.track.files = %s\n", buf);
	get_config_value(config, "train.number.of.cars", &buf);
	fprintf(f, "train.number.of.cars = %s\n", buf);
	get_config_value(config, "train.car.unit", &buf);
	fprintf(f, "train.car.unit = %s\n", buf);

	get_config_value(config, "test.number.of.tracks", &buf);
	fprintf(f, "\ntest.number.of.tracks = %s\n", buf);
	get_config_value(config, "test.track.files", &buf);
	fprintf(f, "test.track.files = %s\n", buf);
	get_config_value(config, "test.number.of.cars", &buf);
	fprintf(f, "test.number.of.cars = %s\n", buf);
	get_config_value(config, "test.car.unit", &buf);
	fprintf(f, "test.car.unit = %s\n", buf);

	fprintf(f, "\n# winner\n");
	fprintf(f, "winner.weights =");
	for (int i = 0; i < len; i++)
		fprintf(f, " %lf", win_w[i]);
	fprintf(f, "\n");
	fprintf(f, "winner.fitness = %lf\n", win_fit);
}

void init_index(struct configuration *config, int *index, char *key) {
	char *buf;
	if (get_config_value(config, key, &buf) == CONF_ERR) return;
	if (sscanf(buf, "%d", index) != 1)
		fprintf(stderr, "%s's value %s is not an integer, using default %d\n", key, buf, *index);
}

void init_indexes(struct configuration *config, int *n_i, int *h_i, int *d_i, int *p_i, int *a_i, int *b_i) {
	init_index(config, n_i, "param.network.index");
	init_index(config, h_i, "param.hidden.activation.index");
	init_index(config, d_i, "param.mutation.deviation.index");
	init_index(config, p_i, "param.mutation.probability.index");
	init_index(config, a_i, "param.alpha.index");
	init_index(config, b_i, "param.beta.index");
}


int main(int argc, char **argv) {
	struct configuration *config, *test_sub_conf;
	int n_neur, n_alpha, n_beta, n_hid, n_dev, n_prob;
	int *n_wall_s, *n_car_s;
	int run = 0, total_runs, w_len, dir_len;
	struct neural_net **p1_nets, **p2_nets;
	double *alphas, *betas, *devs, *probs;
	double *win_w, win_fit;
	char **act_names;
	char dir[50] = {0}, res_dir[60] = {0}, run_file[80];
	struct stat dir_stat = {0};
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
	clock_t diff, start = clock();
	FILE *f;
	int test_ids_len;
	struct track_ids *test_ids;
	act_fun *hids;

	if (argc < 3) {
		fprintf(stderr, "Usage: %s conf_file output_dir\n", argv[0]);
		return 1;
	}

	printf("Loading config...");

	if ((config = config_file(argv[1])) == NULL)
		return 1;

	if (init_dynamic(config, &n_neur, &n_alpha, &n_beta, &n_hid, &n_dev, &n_prob) == 1) return 1;
	total_runs = n_neur * n_alpha * n_beta * n_hid * n_dev * n_prob;

	if (get_config_double_array(config, "param.alpha.values", n_alpha, &alphas) == CONF_ERR) return 1;
	if (get_config_double_array(config, "param.beta.values", n_beta, &betas) == CONF_ERR) return 1;
	if (get_config_string_array(config, "param.hidden.activation.values", n_hid, &act_names) == CONF_ERR) return 1;
	if (get_config_double_array(config, "param.mutation.deviation.values", n_dev, &devs) == CONF_ERR) return 1;
	if (get_config_double_array(config, "param.mutation.probability.values", n_prob, &probs) == CONF_ERR) return 1;

	if (init_nets(config, n_neur, &p1_nets, &p2_nets, &n_wall_s, &n_car_s) == 1) return 1;
	if (init_activation_functions(n_hid, act_names, &hids) == 1) return 1;
	if (init_static(config) == 1) return 1;
	if (init_fitness_tracks(settings.ids_len, settings.ids) == -1) return 1;

	test_sub_conf = get_sub_config(config, "test");
	if (conf_load_tracks(&test_ids, &test_ids_len, test_sub_conf) < 0) return 1;
	init_test_fitness_tracks(test_ids_len, test_ids);

	printf(" done!\n");

	dir_len = strlen(argv[2]);
	memcpy(dir, argv[2], dir_len);
	if (dir[dir_len - 1] == '/')
		dir[dir_len - 1] = '\0';
	if (stat(dir, &dir_stat) == -1)
		mkdir(dir, 0777);
	snprintf(res_dir, 60, "%s/%02d%02d_%02d%02d/", dir, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min);
	if (mkdir(res_dir, 0777) == -1) {
		fprintf(stderr, "Cannot create result dir: %s\n", strerror(errno));
		return 1;
	}

	for (int n_i = 0; n_i < n_neur; n_i++)
		for (int h_i = 0; h_i < n_hid; h_i++)
			for (int d_i = 0; d_i < n_dev; d_i++)
				for (int p_i = 0; p_i < n_prob; p_i++)
					for (int a_i = 0; a_i < n_alpha; a_i++)
						for (int b_i = 0; b_i < n_beta; b_i++) {
							diff = clock() - start;
							printf("Runs: %d/%d, time elapsed: %2ldh %02ldm %02lds\r", run, total_runs,
									diff / CLOCKS_PER_SEC / 3600, diff / CLOCKS_PER_SEC / 60 % 60, diff / CLOCKS_PER_SEC % 60);
							fflush(stdout);
							snprintf(run_dir, 70, "%sresult%04d/", res_dir, ++run);
							if (mkdir(run_dir, 0777) == -1) {
								fprintf(stderr, "Cannot create run dir: %s\n", strerror(errno));
								continue;
							}
							p1_nets[n_i]->hid_act = hids[h_i];
							p2_nets[n_i]->hid_act = hids[h_i];
							if (init_fitness(p1_nets[n_i], p2_nets[n_i], n_wall_s[n_i], n_car_s[n_i], alphas[a_i], betas[b_i], settings.steps)
									== -1) {
								fprintf(stderr, "Cannot init fitness\n");
								continue;
							}
							win_w = learn_parameter_set(p1_nets[n_i], p2_nets[n_i], alphas[a_i], betas[b_i], devs[d_i], probs[p_i]);
							if (win_w == NULL) {
								fprintf(stderr, "Failed to perform learning\n");
								continue;
							}
							if (init_test_fitness(p2_nets[n_i], n_wall_s[n_i], n_car_s[n_i], settings.steps) == -1) {
								fprintf(stderr, "cannot init test fitness\n");
								continue;
							}
							w_len = weights_size(p2_nets[n_i]);
							win_fit = test_fit(w_len, win_w);
							snprintf(run_file, 80, "%sresult.txt", run_dir);
							if ((f = fopen(run_file, "w")) == NULL) {
								fprintf(stderr, "Cannot create result file: %s\n", strerror(errno));
								continue;
							}
							write_result(f, config, p1_nets[n_i], p2_nets[n_i], n_wall_s[n_i], n_car_s[n_i], act_names[h_i],
										devs[d_i], probs[p_i], alphas[a_i], betas[b_i], win_w, win_fit);
							free(win_w);
							fclose(f);
						}
	diff = clock() - start;
	printf("Runs: %d/%d, time elapsed: %2ldh %02ldm %02lds\n", run, total_runs,
			diff / CLOCKS_PER_SEC / 3600, diff / CLOCKS_PER_SEC / 60 % 60, diff / CLOCKS_PER_SEC % 60);
	return 0;
}
