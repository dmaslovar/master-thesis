#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <evol_alg.h>
#include <test.h>
#include <config.h>

#define CONFIG "config.txt"

#define GEN_M 0.0
#define GEN_D 0.0

#define TEST_TRESH 0.001

#define ORIG_ARCH_LAYS 3
#define ORIG_W_LEN 20

int orig_arch[ORIG_ARCH_LAYS] = {3, 3, 2};
double orig_w[ORIG_W_LEN] = {
		0.1, 0.2, 0.3, 0.4,
		0.5, 0.6, 0.7, 0.8,
		0.9, 1.0, 1.1, 1.2,

		1.3, 1.4, 1.5, 1.6,
		1.7, 1.8, 1.9, 2.0
};

struct configuration *glob_config;

int get_w_len(int arch_len, int *arch) {
	int len = 0;
	for (int i = 1; i < arch_len; i++)
		len += (arch[i - 1] + 1) * arch[i];

	return len;
}

void test_random_pop() {
	struct configuration *config = get_sub_config(glob_config, "test1");
	struct population *pop = conf_random_pop(config);
	for (int i = 0; i < ORIG_W_LEN; i++) {
		test_doubles(0.0, pop->pop[0].weights[i], TEST_TRESH);
	}
}

void test_pretrained_pop() {
	struct configuration *config = get_sub_config(glob_config, "test2");
	struct population *pop = conf_pop_from_pretrained(config);
	for (int i = 0; i < ORIG_W_LEN; i++) {
		test_doubles(orig_w[i], pop->pop[0].weights[i], TEST_TRESH);
		test_doubles(orig_w[i], pop->pop[1].weights[i], TEST_TRESH);
	}
}

void test_dif_arch1() {
	double expected_weights[25] = {
		0.1, 0.2, 0.3, 0.4, 0.0,
		0.5, 0.6, 0.7, 0.8, 0.0,
		0.9, 1.0, 1.1, 1.2, 0.0,
		0.0, 0.0, 0.0, 0.0, 0.0,

		1.3, 1.4, 1.5, 1.6, 0.0,
	};
	struct configuration *config = get_sub_config(glob_config, "test3");
	struct population *pop = conf_new_arch_pop_from_pretrained(config);
	for (int i = 0; i < 25; i++) {
		test_doubles(expected_weights[i], pop->pop[0].weights[i], TEST_TRESH);
	}
}

void test_dif_arch2() {
	int new_arch[3] = {2, 4, 3};
	double expected_weights[27] = {
		0.1, 0.2, 0.3,
		0.5, 0.6, 0.7,
		0.9, 1.0, 1.1,
		0.0, 0.0, 0.0,

		1.3, 1.4, 1.5, 1.6, 0.0,
		1.7, 1.8, 1.9, 2.0, 0.0,
		0.0, 0.0, 0.0, 0.0, 0.0
	};
	struct population *pop = new_arch_pop_from_pretrained(1, ORIG_W_LEN, orig_w, 27, GEN_M, GEN_D,
								ORIG_ARCH_LAYS, orig_arch, 3, new_arch);
	for (int i = 0; i < 27; i++) {
		test_doubles(expected_weights[i], pop->pop[0].weights[i], TEST_TRESH);
	}
}

int main(int argc, char **argv) {
	void (*functions[4])();
	glob_config = config_file(CONFIG);

	srand(time(NULL));

	functions[0] = test_random_pop;
	functions[1] = test_pretrained_pop;
	functions[2] = test_dif_arch1;
	functions[3] = test_dif_arch2;
	run_tests(functions, 4);

	return 0;
}
