#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <evol_alg.h>
#include <test.h>

#define GEN_M 0.0
#define GEN_D 0.0

#define TEST_TRESH 0.001

#define ORIG_ARCH_LAYS 3
#define ORIG_W_LEN 20

int orig_arch[ORIG_ARCH_LAYS] = {3, 3, 2};
double orig_w[ORIG_W_LEN] = {
		0.1, 0.2, 0.3, 0.4,
		0.5, 0.6, 0.7, 0.8,
		0.9, 1.0, 1.1, 1.2,

		1.3, 1.4, 1.5, 1.6,
		1.7, 1.8, 1.9, 2.0
};

int get_w_len(int arch_len, int *arch) {
	int len = 0;
	for (int i = 1; i < arch_len; i++)
		len += (arch[i - 1] + 1) * arch[i];

	return len;
}

void test_random_pop() {
	struct population *pop = random_pop(1, ORIG_W_LEN, GEN_M, GEN_D);
	for (int i = 0; i < ORIG_W_LEN; i++) {
		test_doubles(0.0, pop->pop[0].weights[i], TEST_TRESH);
	}
}

void test_pretrained_pop() {
	struct population *pop = pop_from_pretrained(2, ORIG_W_LEN, orig_w, GEN_M, GEN_D);
	for (int i = 0; i < ORIG_W_LEN; i++) {
		test_doubles(orig_w[i], pop->pop[0].weights[i], TEST_TRESH);
		test_doubles(orig_w[i], pop->pop[1].weights[i], TEST_TRESH);
	}
}

void test_same_arch() {
	struct population *pop = new_arch_pop_from_pretrained(1, ORIG_W_LEN, orig_w, ORIG_W_LEN, GEN_M, GEN_D,
								ORIG_ARCH_LAYS, orig_arch, ORIG_ARCH_LAYS, orig_arch);
	for (int i = 0; i < ORIG_W_LEN; i++) {
		test_doubles(orig_w[i], pop->pop[0].weights[i], TEST_TRESH);
	}
}

void test_smaller_arch1() {
	int new_arch[3] = {2, 2, 1};
	double expected_weights[9] = {
		0.1, 0.2, 0.3,
		0.5, 0.6, 0.7,

		1.3, 1.4, 1.5
	};
	struct population *pop = new_arch_pop_from_pretrained(1, ORIG_W_LEN, orig_w, 9, GEN_M, GEN_D,
								ORIG_ARCH_LAYS, orig_arch, 3, new_arch);
	for (int i = 0; i < 9; i++) {
		test_doubles(expected_weights[i], pop->pop[0].weights[i], TEST_TRESH);
	}
}

void test_smaller_arch2() {
	int new_arch[2] = {3, 2};
	double expected_weights[8] = {
		0.1, 0.2, 0.3, 0.4,
		0.5, 0.6, 0.7, 0.8,
	};
	struct population *pop = new_arch_pop_from_pretrained(1, ORIG_W_LEN, orig_w, 8, GEN_M, GEN_D,
								ORIG_ARCH_LAYS, orig_arch, 2, new_arch);
	for (int i = 0; i < 8; i++) {
		test_doubles(expected_weights[i], pop->pop[0].weights[i], TEST_TRESH);
	}
}

void test_bigger_arch1() {
	int new_arch[4] = {3, 3, 2, 1};
	double expected_weights[23] = {
		0.1, 0.2, 0.3, 0.4,
		0.5, 0.6, 0.7, 0.8,
		0.9, 1.0, 1.1, 1.2,

		1.3, 1.4, 1.5, 1.6,
		1.7, 1.8, 1.9, 2.0,

		0.0, 0.0, 0.0
	};
	struct population *pop = new_arch_pop_from_pretrained(1, ORIG_W_LEN, orig_w, 23, GEN_M, GEN_D,
								ORIG_ARCH_LAYS, orig_arch, 4, new_arch);
	for (int i = 0; i < 23; i++) {
		test_doubles(expected_weights[i], pop->pop[0].weights[i], TEST_TRESH);
	}
}

void test_bigger_arch2() {
	int new_arch[4] = {4, 4, 3, 2};
	double expected_weights[43] = {
		0.1, 0.2, 0.3, 0.4, 0.0,
		0.5, 0.6, 0.7, 0.8, 0.0,
		0.9, 1.0, 1.1, 1.2, 0.0,
		0.0, 0.0, 0.0, 0.0, 0.0,

		1.3, 1.4, 1.5, 1.6, 0.0,
		1.7, 1.8, 1.9, 2.0, 0.0,
		0.0, 0.0, 0.0, 0.0, 0.0,

		0.0, 0.0, 0.0, 0.0,
		0.0, 0.0, 0.0, 0.0
	};
	struct population *pop = new_arch_pop_from_pretrained(1, ORIG_W_LEN, orig_w, 43, GEN_M, GEN_D,
								ORIG_ARCH_LAYS, orig_arch, 4, new_arch);
	for (int i = 0; i < 43; i++) {
		test_doubles(expected_weights[i], pop->pop[0].weights[i], TEST_TRESH);
	}
}

void test_dif_arch1() {
	int new_arch[3] = {4, 4, 1};
	double expected_weights[25] = {
		0.1, 0.2, 0.3, 0.4, 0.0,
		0.5, 0.6, 0.7, 0.8, 0.0,
		0.9, 1.0, 1.1, 1.2, 0.0,
		0.0, 0.0, 0.0, 0.0, 0.0,

		1.3, 1.4, 1.5, 1.6, 0.0,
	};
	struct population *pop = new_arch_pop_from_pretrained(1, ORIG_W_LEN, orig_w, 25, GEN_M, GEN_D,
								ORIG_ARCH_LAYS, orig_arch, 3, new_arch);
	for (int i = 0; i < 25; i++) {
		test_doubles(expected_weights[i], pop->pop[0].weights[i], TEST_TRESH);
	}
}

void test_dif_arch2() {
	int new_arch[3] = {2, 4, 3};
	double expected_weights[27] = {
		0.1, 0.2, 0.3,
		0.5, 0.6, 0.7,
		0.9, 1.0, 1.1,
		0.0, 0.0, 0.0,

		1.3, 1.4, 1.5, 1.6, 0.0,
		1.7, 1.8, 1.9, 2.0, 0.0,
		0.0, 0.0, 0.0, 0.0, 0.0
	};
	struct population *pop = new_arch_pop_from_pretrained(1, ORIG_W_LEN, orig_w, 27, GEN_M, GEN_D,
								ORIG_ARCH_LAYS, orig_arch, 3, new_arch);
	for (int i = 0; i < 27; i++) {
		test_doubles(expected_weights[i], pop->pop[0].weights[i], TEST_TRESH);
	}
}

int main(int argc, char **argv) {
	void (*functions[9])();

	srand(time(NULL));

	functions[0] = test_random_pop;
	functions[1] = test_pretrained_pop;
	functions[2] = test_same_arch;
	functions[3] = test_smaller_arch1;
	functions[4] = test_smaller_arch2;
	functions[5] = test_bigger_arch1;
	functions[6] = test_bigger_arch2;
	functions[7] = test_dif_arch1;
	functions[8] = test_dif_arch2;
	run_tests(functions, 9);

	return 0;
}
