/*
 *
 * Library which contains the k-tournament elimination algorithm
 *
 */
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <evol_alg.h>
#include <gauss.h>

/* Stored initialisation settings */
struct _evol_settings {
	double mean;
	double dev;
	double mut_prob;
} _evol_alg_settings;

/* Perform k-tournament, and put the best and the worst index of the selected in res */
void _k_tournament(int *res, int k, int pop_size, struct individual *pop) {
	double worst_fit = DBL_MAX;
	double best_fit = -DBL_MAX;
	int index;

	for (int i = 0; i < k; i++) {
		index = rand() % pop_size;

		if (pop[index].fit <= worst_fit) {
			worst_fit = pop[index].fit;
			res[_WORST_IND] = index;
		}

		if (pop[index].fit >= best_fit) {
			best_fit = pop[index].fit;
			res[_BEST_IND] = index;
		}
	}
}

struct population *_malloc_pop(int n) {
	struct population *pop = (struct population*) malloc(sizeof(struct population));
	pop->size = n;
	pop->pop = (struct individual*) malloc(n * sizeof(struct individual));

	return pop;
}

/* Generate random population with gaussian distribution */
struct population *random_pop(int n, int w_size, double mean, double dev) {
	struct population *pop = _malloc_pop(n);

	for (int i = 0; i < n; i++) {
		pop->pop[i].fit = 0.0;
		pop->pop[i].size = w_size;
		pop->pop[i].weights = (double*) malloc(w_size * sizeof(double));

		for (int j = 0; j < w_size; j++)
			pop->pop[i].weights[j] = mean + dev * rand_gauss();
	}

	return pop;
}

struct population *conf_random_pop(struct configuration *config) {
	int n, w_size;
	double mean, dev;

	if (get_config_integer(config, _EVOL_POP_SIZE, &n) == CONF_ERR) return NULL;
	if (get_config_integer(config, _CHROMOSOME_LENGTH, &w_size) == CONF_ERR) return NULL;
	if (get_config_double(config, _CHROMOSOME_MEAN, &mean) == CONF_ERR) return NULL;
	if (get_config_double(config, _CHROMOSOME_DEV, &dev) == CONF_ERR) return NULL;

	return random_pop(n, w_size, mean, dev);
}

struct population *pop_from_pretrained(int n, int w_size, double *w, double mean, double dev) {
	struct population *pop = _malloc_pop(n);

	pop->pop[0].fit = 0.0;
	pop->pop[0].size = w_size;
	pop->pop[0].weights = (double*) malloc(w_size * sizeof(double));
	memcpy(pop->pop[0].weights, w, w_size * sizeof(double));

	for (int i = 1; i < n; i++) {
		pop->pop[i].fit = 0.0;
		pop->pop[i].size = w_size;
		pop->pop[i].weights = (double*) malloc(w_size * sizeof(double));

		for (int j = 0; j < w_size; j++)
			pop->pop[i].weights[j] = w[j] + mean + dev * rand_gauss();
	}

	return pop;
}

struct population *conf_pop_from_pretrained_w(struct configuration *config, int w_size, double *w) {
	int n;
	double mean, dev;

	if (get_config_integer(config, _EVOL_POP_SIZE, &n) == CONF_ERR) return NULL;
	if (get_config_double(config, _CHROMOSOME_MEAN, &mean) == CONF_ERR) return NULL;
	if (get_config_double(config, _CHROMOSOME_DEV, &dev) == CONF_ERR) return NULL;

	return pop_from_pretrained(n, w_size, w, mean, dev);
}

struct population *conf_pop_from_pretrained(struct configuration *config) {
	int w_size;
	double *w;

	if (get_config_integer(config, _CHROMOSOME_LENGTH, &w_size) == CONF_ERR) return NULL;
	if (get_config_double_array(config, _PRETRAINED_CHROMOSOME, w_size, &w) == CONF_ERR) return NULL;

	return conf_pop_from_pretrained_w(config, w_size, w);
}

struct population *new_arch_pop_from_pretrained(int n, int old_w_size, double *weights, int new_w_size,
				double mean, double dev, int old_arch_lays, int *old_arch, int new_arch_lays, int *new_arch) {

	struct population *pop = _malloc_pop(n);
	int pretrained_lays = old_arch_lays < new_arch_lays ? old_arch_lays : new_arch_lays;
	int new_lays = pretrained_lays + new_arch_lays - old_arch_lays;

	for (int i = 0; i < n; i++) {
		int w_i = 0, old_n_i = 0;
		pop->pop[i].fit = 0.0;
		pop->pop[i].size = new_w_size;
		pop->pop[i].weights = (double*) malloc(new_w_size * sizeof(double));

		for (int lay = 1; lay < pretrained_lays; lay++) {
			int old_w_i = 0;
			int pretrained_neurs = old_arch[lay] < new_arch[lay] ? old_arch[lay] : new_arch[lay];
			int new_neurs = new_arch[lay] - old_arch[lay];
			int pretrained_ws = (old_arch[lay-1] < new_arch[lay-1] ? old_arch[lay-1] : new_arch[lay-1]) + 1;
			int new_ws = new_arch[lay-1] - old_arch[lay-1];
			int new_neur_size = new_arch[lay-1] + 1;
			int old_neur_size = old_arch[lay-1] + 1;

			for (int neur = 0; neur < pretrained_neurs; neur++) {
				for (int w = 0; w < pretrained_ws; w++)
					pop->pop[i].weights[w_i++] = weights[old_n_i + old_w_i + w];
				old_w_i += old_neur_size;
				for (int w = 0; w < new_ws; w++)
					pop->pop[i].weights[w_i++] = mean + dev * rand_gauss();
			}

			old_n_i += (old_arch[lay-1] + 1) * old_arch[lay];

			for (int neur = 0; neur < new_neurs; neur++)
				for (int j = 0; j < new_neur_size; j++)
					pop->pop[i].weights[w_i++] = mean + dev * rand_gauss();
		}

		for (int lay = pretrained_lays; lay < new_lays; lay++) {
			int lay_size = (new_arch[lay-1] + 1) * new_arch[lay];
			for (int j = 0; j < lay_size; j++)
				pop->pop[i].weights[w_i++] = mean + dev * rand_gauss();
		}
	}

	return pop;
}

struct population *conf_new_arch_pop_from_pretrained_w(struct configuration *config, int old_w_size, double *w) {
	int n, new_w_size, old_arch_lays, new_arch_lays, *old_arch, *new_arch;
	double mean, dev;

	if (get_config_integer(config, _EVOL_POP_SIZE, &n) == CONF_ERR) return NULL;
	if (get_config_integer(config, _CHROMOSOME_NEW_LENGTH, &new_w_size) == CONF_ERR) return NULL;
	if (get_config_integer(config, _OLD_ARCH_LAYS, &old_arch_lays) == CONF_ERR) return NULL;
	if (get_config_integer(config, _NEW_ARCH_LAYS, &new_arch_lays) == CONF_ERR) return NULL;
	if (get_config_integer_array(config, _OLD_ARCH, old_arch_lays, &old_arch) == CONF_ERR) return NULL;
	if (get_config_integer_array(config, _NEW_ARCH, new_arch_lays, &new_arch) == CONF_ERR) return NULL;
	if (get_config_double(config, _CHROMOSOME_MEAN, &mean) == CONF_ERR) return NULL;
	if (get_config_double(config, _CHROMOSOME_DEV, &dev) == CONF_ERR) return NULL;

	return new_arch_pop_from_pretrained(n, old_w_size, w, new_w_size, mean, dev, old_arch_lays, old_arch, new_arch_lays, new_arch);
}


struct population *conf_new_arch_pop_from_pretrained(struct configuration *config) {
	int old_w_size;
	double *w;

	if (get_config_integer(config, _CHROMOSOME_OLD_LENGTH, &old_w_size) == CONF_ERR) return NULL;
	if (get_config_double_array(config, _PRETRAINED_CHROMOSOME, old_w_size, &w) == CONF_ERR) return NULL;

	return conf_new_arch_pop_from_pretrained_w(config, old_w_size, w);
}

/* Take parent's weights and mutate it in the child */
void _gaussian_mutation(int size, double *parent, double *child) {
	for (int i = 0; i < size; i++) {
		child[i] = parent[i];
		if (rand_scaled() <= _evol_alg_settings.mut_prob)
			child[i] += _evol_alg_settings.mean + _evol_alg_settings.dev * rand_gauss();
	}
}

/* Initialise new evolutionary algorithm. mean, dev, and mut_prob are for gaussian mutation */
struct evolution *new_evolution(int k, double (*fit)(int, double*), double mean, double dev, double mut_prob, long gens, struct population *pop) {
	struct evolution *result;
	_evol_alg_settings.mean = mean;
	_evol_alg_settings.dev = dev;
	_evol_alg_settings.mut_prob = mut_prob;

	result = (struct evolution*) malloc(sizeof(struct evolution));
	result->k = k;
	result->fit = fit;
	result->mut = _gaussian_mutation;
	result->gens = gens;
	result->pop = pop;

	return result;
}

struct evolution *conf_new_evolution_skip_pop(struct configuration *config) {
	int k;
	long gens;
	double mean, dev, prob;

	if (get_config_integer(config, _EVOL_TOURN, &k) == CONF_ERR) return NULL;
	if (get_config_long(config, _EVOL_GENS, &gens) == CONF_ERR) return NULL;
	if (get_config_double(config, _EVOL_MUT_MEAN, &mean) == CONF_ERR) return NULL;
	if (get_config_double(config, _EVOL_MUT_DEV, &dev) == CONF_ERR) return NULL;
	if (get_config_double(config, _EVOL_MUT_PROB, &prob) == CONF_ERR) return NULL;

	return new_evolution(k, NULL, mean, dev, prob, gens, NULL);
}

struct evolution *conf_new_evolution(struct configuration *config) {
	struct evolution *evol = conf_new_evolution_skip_pop(config);
	struct population *pop;
	char *buf;

	if (get_config_value_err(config, _EVOL_POP_METHOD, &buf) == CONF_ERR) return NULL;

	if (!strcmp(buf, _EVOL_POP_METHOD_RAND))
		pop = conf_random_pop(config);
	else if (!strcmp(buf, _EVOL_POP_METHOD_PRETRAINED))
		pop = conf_pop_from_pretrained(config);
	else if (!strcmp(buf, _EVOL_POP_METHOD_NEW_ARCH_PRETRAINED))
		pop = conf_new_arch_pop_from_pretrained(config);
	else {
		fprintf(stderr, "%s not a valid value for %s\n", buf, _EVOL_POP_METHOD);
		return NULL;
	}

	if (pop == NULL) {
		fprintf(stderr, "conf_new_evolution: cannot init pop\n");
		return NULL;
	}

	evol->pop = pop;

	return evol;
}

void release_evolution(struct evolution *evol) {
	for (int i = 0; i < evol->pop->size; i++)
		free(evol->pop->pop[i].weights);
	free(evol->pop);
	free(evol);
}

void _log_nothing(int generation, double best_fit) {}

/* Perform evolutionary alg for gens generations on pop */
void learn_evol(struct evolution *alg, void (*log)(int, double)) {
	int k_res[2];
	double best_fit = -DBL_MAX;
	int pop_size = alg->pop->size;
	struct individual *pop = alg->pop->pop;
	if (log == NULL)
		log = &_log_nothing;

	for (int i = 0; i < pop_size; i++) {
		pop[i].fit = alg->fit(pop[i].size, pop[i].weights);
		if (pop[i].fit > best_fit)
			best_fit = pop[i].fit;
	}

	for (long i = 0; i <= alg->gens; i++) {
		_k_tournament(k_res, alg->k, pop_size, pop);
		alg->mut(pop[k_res[_BEST_IND]].size, pop[k_res[_BEST_IND]].weights, pop[k_res[_WORST_IND]].weights);
		pop[k_res[_WORST_IND]].fit = alg->fit(pop[k_res[_WORST_IND]].size, pop[k_res[_WORST_IND]].weights);

		if (pop[k_res[_WORST_IND]].fit > best_fit)
			best_fit = pop[k_res[_WORST_IND]].fit;

		log(i, best_fit);
	}
}
