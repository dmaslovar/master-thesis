test1.population.size = 1
test1.chromosome.length = 20
test1.chromosome.mean = 0.0
test1.chromosome.deviation = 0.0

test2.population.size = 2
test2.chromosome.length = 20
test2.chromosome.mean = 0.0
test2.chromosome.deviation = 0.0
test2.pretrained.chromosome = 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0

test3.population.size = 1
test3.old.chromosome.length = 20
test3.new.chromosome.length = 25
test3.old.architecture.length = 3
test3.old.architecture = 3 3 2
test3.new.architecture.length = 3
test3.new.architecture = 4 4 1
test3.chromosome.mean = 0.0
test3.chromosome.deviation = 0.0
test3.pretrained.chromosome = 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0

test4.population.size = 1
test4.old.chromosome.length = 20
test4.new.chromosome.length = 27
test4.old.architecture.length = 3
test4.old.architecture = 3 3 2
test4.new.architecture.length = 3
test4.new.architecture = 2 4 3
test4.chromosome.mean = 0.0
test4.chromosome.deviation = 0.0
test4.pretrained.chromosome = 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0
