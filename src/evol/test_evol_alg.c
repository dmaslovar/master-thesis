#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <evol_alg.h>
#include <test.h>

#define W_SIZE 10
#define POP 50
#define TOURN_K 3
#define GENS 10000

#define MUT_M 0.0
#define MUT_D 1.0
#define MUT_P 0.15

#define GEN_M 5.0
#define GEN_D 10.0

#define TEST_TRESH 0.1
#define F_NUM 1

struct individual *best;

double fitness(int size, double *weights) {
	double res = 0.0;
	for (int i = 0; i < size; i++)
		res -= (weights[i] - i) * (weights[i] - i);
	return res;
}



void print_ind(struct individual *unit) {
	for (int i = 0; i < unit->size; i++) {
		printf("%lf ", unit->weights[i]);
	}
}

void test_best() {
	for (int i = 0; i < best->size; i++) {
		test_doubles(best->weights[i], (double) i, TEST_TRESH);
	}
}

void evol_log(int gen, double best_fit) {
	if (gen % 100 != 0) return;
	printf("gen: %4d/%d, best: %lf\r", gen, GENS, best_fit);
}

int main(int argc, char **argv) {
	int best_i;
	double best_fit = -10000000;
	void (*functions[F_NUM])();

	srand(time(NULL));
	struct population *pop = random_pop(POP, W_SIZE, GEN_M, GEN_D);
	struct evolution *algorithm = new_evolution(TOURN_K, fitness, MUT_M, MUT_D, MUT_P, GENS, pop);
	learn_evol(algorithm, evol_log);
	printf("\n");

	for (int i = 0; i < POP; i++) {
		if (pop->pop[i].fit > best_fit) {
			best_i = i;
			best_fit = pop->pop[i].fit;
		}
	}

	best = pop->pop + best_i;
	functions[0] = test_best;
	run_tests(functions, F_NUM);

	return 0;
}
