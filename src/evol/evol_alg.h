/*
 *
 * Library which contains the k-tournament elimination algorithm
 *
 */
#ifndef _EVOL_ALG_H
#define _EVOL_ALG_H

#include <config.h>

#define _BEST_IND 0 /* Index of the best individiual */
#define _WORST_IND 1 /* Index of the worst individual */

#define _EVOL_POP_SIZE "population.size"
#define _CHROMOSOME_LENGTH "chromosome.length"
#define _CHROMOSOME_MEAN "chromosome.mean"
#define _CHROMOSOME_DEV "chromosome.deviation"
#define _PRETRAINED_CHROMOSOME "pretrained.chromosome"

#define _CHROMOSOME_OLD_LENGTH "old.chromosome.length"
#define _CHROMOSOME_NEW_LENGTH "new.chromosome.length"
#define _OLD_ARCH_LAYS "old.architecture.length"
#define _NEW_ARCH_LAYS "new.architecture.length"
#define _OLD_ARCH "old.architecture"
#define _NEW_ARCH "new.architecture"

#define _EVOL_POP_METHOD "population.generation.method"
#define _EVOL_POP_METHOD_RAND "random.population"
#define _EVOL_POP_METHOD_PRETRAINED "population.from.pretrained"
#define _EVOL_POP_METHOD_NEW_ARCH_PRETRAINED "new.architecture.from.pretrained"

#define _EVOL_MUT_PROB "mutation.probability"
#define _EVOL_MUT_MEAN "mutation.mean"
#define _EVOL_MUT_DEV "mutation.deviation"
#define _EVOL_GENS "number.of.generations"
#define _EVOL_TOURN "k.tournament"

/* Population individual */
struct individual {
	double fit;
	int size;
	double *weights;
};

struct population {
	int size;
	struct individual *pop;
};

/* Evolutionary algorithm */
struct evolution {
	int k; /* Number of individuals considered selected for the tournament */
	double (*fit)(int, double *); /* Fitness function (size, weights) */
	void (*mut)(int, double *, double *); /* Mutation function (size, parent, child) */
	long gens;
	struct population *pop;
};

/* Generate random population with gaussian distribution */
struct population *random_pop(int n, int w_size, double mean, double dev);

struct population *conf_random_pop(struct configuration *config);

struct population *pop_from_pretrained(int n, int w_size, double *w, double mean, double dev);

struct population *conf_pop_from_pretrained_w(struct configuration *config, int w_size, double *w);

struct population *conf_pop_from_pretrained(struct configuration *config);

struct population *new_arch_pop_from_pretrained(int n, int old_w_size, double *weights, int new_w_size,
				double mean, double dev, int old_arch_lays, int *old_arch, int new_arch_lays, int *new_arch);

struct population *conf_new_arch_pop_from_pretrained_w(struct configuration *config, int old_w_size, double *w);

struct population *conf_new_arch_pop_from_pretrained(struct configuration *config);

/* Initialise new evolutionary algorithm. mean, dev, and mut_prob are for gaussian mutation */
struct evolution *new_evolution(int k, double (*fit)(int, double*), double mean, double dev, double mut_prob, long gens, struct population *pop);

struct evolution *conf_new_evolution(struct configuration *config);

struct evolution *conf_new_evolution_skip_pop(struct configuration *config);

void release_evolution(struct evolution *evol);

/* Perform evolutionary alg for gens generations on pop */
void learn_evol(struct evolution *alg, void (*log)(int, double));

#endif
