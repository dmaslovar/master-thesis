#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "gauss.h"

int main(int argc, char **argv) {
	int count[10];
	double rand_val;

	srand(time(NULL));
	memset(count, 0, sizeof(count));

	for (int i = 0; i < 100000; i++) {
		rand_val = rand_gauss();

		if (rand_val >= -1.0 && rand_val <= -0.8)
			count[0]++;
		else if (rand_val > -0.8 && rand_val <= -0.6)
			count[1]++;
		else if (rand_val > -0.6 && rand_val <= -0.4)
			count[2]++;
		else if (rand_val > -0.4 && rand_val <= -0.2)
			count[3]++;
		else if (rand_val > -0.2 && rand_val <= 0.0)
			count[4]++;
		else if (rand_val > 0.0 && rand_val <= 0.2)
			count[5]++;
		else if (rand_val > 0.2 && rand_val <= 0.4)
			count[6]++;
		else if (rand_val > 0.4 && rand_val <= 0.6)
			count[7]++;
		else if (rand_val > 0.6 && rand_val <= 0.8)
			count[8]++;
		else if (rand_val > 0.8 && rand_val <= 1.0)
			count[9]++;
	}

	for (int i = 0; i < 10; i++) {
		printf("%d\n", count[i]);
	}

	return 0;
}
