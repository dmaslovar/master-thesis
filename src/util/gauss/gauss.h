/*
 *
 * Utility library for generating pseudo-random gaussians
 *
 */
#ifndef _GAUSS_H
#define _GAUSS_H

/* Get uniform from [0, 1] */
double rand_scaled();

/* Get gaussian random from [-1, 1] */
double rand_gauss();

#endif
