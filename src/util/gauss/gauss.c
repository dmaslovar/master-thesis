#include <stdlib.h>
#include "gauss.h"

/* Get uniform random from [0, 1] */
double rand_scaled() {
	return (double) rand() / RAND_MAX;
}

/* Get gaussian random from [-1, 1] */
double rand_gauss() {
	double res = 0.0;

	for (int i = 0; i < 12; i++) {
		res += rand_scaled();
	}

	return (res / 6.0 - 1.0) * 3.5;
}
