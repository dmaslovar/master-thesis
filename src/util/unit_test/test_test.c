/*
 *
 * File for testing test.c
 *
 */
#include "test.h"
#include <string.h>
#include <stdio.h>
#define FUNCTION_NUM 3 // number of functions which will be tested

/* Just a dummy struct, really */
struct dummy {
	int id;
	char name[50];
} dummy1, dummy2;

/* Initialise some global stuff */
void init_global() {
	dummy1.id = 8;
	dummy2.id = 8;
	sprintf(dummy1.name, "a string");
	sprintf(dummy2.name, "a string!");
}

/* Test some stuff */
void my_function_1() {
	test_doubles(10.748, 10.749, 0.001); // should fail
	test_doubles(10.748, 10.749, 0.1); // should succeed
}

/* Test some other stuff */
void my_function_2() {
	test_bytes((void*) &dummy1, (void*) &dummy2, sizeof(struct dummy)); // should fail
	test_bytes((void*) &dummy1, (void*) &dummy2, sizeof(int) + strlen(dummy1.name)); // should succeed
}

/* Test some other other stuff */
void my_function_3() {
	test_condition(124 > 76); // should succeed
	test_condition(124 == 76); // should fail
}

int main(int argc, char** argv) {
	void (*functions[FUNCTION_NUM])();

	functions[0] = my_function_1;
	functions[1] = my_function_2;
	functions[2] = my_function_3;

	printf("testing tested_file\n");
	init_global();

	run_tests(functions, FUNCTION_NUM);

	return 0; // typically return run_tests(functions, FUNCTION_NUM);
}
