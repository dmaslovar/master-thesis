/*
 *
 * Utility library for unit testing of C code
 *
 */
#ifndef _UNIT_TEST_H
#define _UNIT_TEST_H
#define UNIT_TEST_SUCCESS 0
#define UNIT_TEST_FAIL 1

/* test whether condition is true */
void test_condition(int condition);

/* assert the absolute difference between d1 and d2 is lesser than tresh */
void test_doubles(double d1, double d2, double tresh);

/* assert that first size bytes of p1 and p2 are equal */
void test_bytes(void *p1, void *p2, long size);

/* run functions which contain assertions contained above and print statistics
 * returns either UNIT_TEST_SUCCESS if all test succeed, or UNIT_TEST_FAIL if at least one test failed
 */
int run_tests(void (**funs) (), int size);

#endif
