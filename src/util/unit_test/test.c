/*
 *
 * Utility library for unit testing of C code
 *
 */
#include "test.h"
#include <stdio.h>
#include <math.h>

/* current test statistics */
struct _test_run {
	int tests; // number of test functions called during this testing
	int success; // number of successful tests
	int fails; // number of failed tests
	int cur_fun; // index of the current function
	int cur_test; // index of the test within the current function
} _current_test;

/* test whether condition is true */
void test_condition(int condition) {
	_current_test.tests++;
	_current_test.cur_test++;

	if (condition) {
		_current_test.success++;
		return;
	}

	_current_test.fails++;
	dprintf(2, "[TEST FAIL] test_condition (function %d, test %d) failed\n",
		_current_test.cur_fun, _current_test.cur_test);

}

/* assert the absolute difference between d1 and d2 is lesser than tresh */
void test_doubles(double d1, double d2, double tresh) {
	_current_test.tests++;
	_current_test.cur_test++;

	if (fabs(d1 - d2) < tresh) {
		_current_test.success++;
		return;
	}

	_current_test.fails++;
	dprintf(2, "[TEST FAIL] test_doubles (function %d, test %d) failed\n",
		_current_test.cur_fun, _current_test.cur_test);
}

/* assert that first size bytes of p1 and p2 are equal */
void test_bytes(void *p1, void *p2, long size) {
	char *blob1 = (char*) p1;
	char *blob2 = (char*) p2;

	_current_test.tests++;
	_current_test.cur_test++;

	for (long i = 0; i < size; i++) {
		if (blob1[i] != blob2[i]) {
			_current_test.fails++;
			dprintf(2, "[TEST FAIL] test_bytes (function %d, test %d) failed\n",
				_current_test.cur_fun, _current_test.cur_test);
			return;
		}
	}

	_current_test.success++;
}

/* run functions which contain assertions contained above and print statistics
 * returns either UNIT_TEST_SUCCESS if all tests succeeded, or UNIT_TEST_FAIL if at least one test failed
 */
int run_tests(void (**funs) (), int size) {
	printf("===========TESTS STARTED===========\n");

	for (int i = 0; i < size; i++) {
		_current_test.cur_fun++;
		_current_test.cur_test = 0;

		(*funs[i])();
	}

	printf("===========TESTS FINISHED===========\n");
	printf("runs: %d\n", _current_test.tests);
	printf("successes: %d\n", _current_test.success);
	printf("fails: %d\n", _current_test.fails);

	if (_current_test.fails != 0) return UNIT_TEST_FAIL;

	return UNIT_TEST_SUCCESS;
}
