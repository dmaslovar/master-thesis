#include <stdio.h>
#include "config.h"

int main(int argc, char **argv) {
	char *val1, *val2 = NULL, **array, *val3;
	int int_len, long_len, double_len;
	int *int_val;
	long *long_val;
	double *double_val;
	struct configuration *config = config_file("res.conf");
	struct configuration *sub_conf = get_sub_config(config, "sub");
	config_to_stream(sub_conf, stdout);

	get_config_value(sub_conf, "property.1", &val1);
	get_config_value(config, "non.existent", &val2);

	printf("val of property.1 is %s and of non.existent is %s\n", val1, val2);

	get_config_value(config, "some word key", &val2);
	printf("some word: %s\n", val2);
	get_config_string_array(config, "some word key", 3, &array);
	printf("some word: %s\nsome word array:", val2);
	for (int i = 0; i < 3; i++)
		printf(" %s", array[i]);
	printf("\n");

	get_config_integer(config, "int.len", &int_len);
	get_config_integer(config, "long.len", &long_len);
	get_config_integer(config, "double.len", &double_len);

	get_config_integer_array(config, "int.val", int_len, &int_val);
	get_config_long_array(config, "long.val", long_len, &long_val);
	get_config_double_array(config, "double.val", double_len, &double_val);

	get_config_value(config, "int.val", &val1);
	get_config_value(config, "long.val", &val2);
	get_config_value(config, "double.val", &val3);

	printf("int {%s;", val1);
	for (int i = 0; i < int_len; i++) printf(" %d", int_val[i]);
	printf("}\nlong {%s;", val2);
	for (int i = 0; i < long_len; i++) printf(" %ld", long_val[i]);
	printf("}\ndouble {%s;", val3);
	for (int i = 0; i < double_len; i++) printf(" %lf", double_val[i]);
	printf("}\n");

	return 0;
}
