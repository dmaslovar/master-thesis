#ifndef _CONFIG_H
#define _CONFIG_H
#include <stdio.h>

#define CONF_OK 1
#define CONF_ERR 0

struct _property {
	char *key;
	char *value;
	struct _property *next;
};

struct configuration {
	struct _property *head;
	int size;
};

struct configuration *config_file(char *filename);

int config_to_stream(struct configuration *config, FILE* f);

int config_to_file(struct configuration *config, char *filename);

struct configuration *get_sub_config(struct configuration *config, char *prefix);

int get_config_value(struct configuration *config, char *key, char **value);

int get_config_value_err(struct configuration *config, char *key, char **value);

int get_config_integer(struct configuration *config, char *key, int *i);

int get_config_long(struct configuration *config, char *key, long *l);

int get_config_double(struct configuration *config, char *key, double *d);

int get_config_string(struct configuration *config, char *key, char **value);

int get_config_string_array(struct configuration *config, char *key, int len, char ***arr);

int get_config_integer_array(struct configuration *config, char *key, int len, int **arr);

int get_config_long_array(struct configuration *config, char *key, int len, long **arr);

int get_config_double_array(struct configuration *config, char *key, int len, double **arr);

void free_config(struct configuration *config);

#endif
