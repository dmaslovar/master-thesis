#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "config.h"

int _read_property(char *line, ssize_t len, char **key, char **value) {
	int key_off = 0, eq_off, val_off;
	int key_len, val_len;
	int i = 0;

	for (; i < len; i++)
		if (line[i] != ' ' && line[i] != '\t')
			break;
	key_off = i;

	for (; i < len; i++)
		if (line[i] == '=')
			break;
	eq_off = i;
	if (eq_off == len) {
		fprintf(stderr, "_read_property: '=' not found in %s", line);
		return -1;
	}

	i++;

	for (; i < len; i++)
		if (line[i] != ' ' && line[i] != '\t')
			break;
	val_off = i;

	for (key_len = eq_off - 1; key_len >= 0; key_len--)
		if (line[key_len] != ' ' && line[key_len] != '\t')
			break;
	key_len = key_len - key_off + 1;

	for (val_len = len - 2; val_len >= 0; val_len--)
		if (line[val_len] != ' ' && line[val_len] != '\t')
			break;
	val_len = val_len - val_off + 1;

//	printf("k_o=%d, k_l=%d, e_o=%d, v_o=%d, v_l=%d, tot=%zu\n", key_off, key_len, eq_off, val_off, val_len, len);

	char *key_mem = (char*) malloc(key_len + 1);
	char *value_mem = (char*) malloc(val_len + 1);

	memcpy(key_mem, line + key_off, key_len);
	key_mem[key_len] = 0;

	memcpy(value_mem, line + val_off, val_len);
	value_mem[val_len] = 0;

	*key = key_mem;
	*value = value_mem;

	return 0;
}

struct configuration *config_file(char *filename) {
	FILE *f;
	char *line = NULL, *key, *value;
	size_t len = 0;
	ssize_t read;
	int first_char;

	struct _property *prop, *cur;
	struct configuration *config = (struct configuration*) malloc(sizeof(struct configuration));
	config->size = 0;
	config->head = NULL;

	if ((f = fopen(filename, "r")) == NULL) {
		fprintf(stderr, "config_file: cannot open %s: %s\n", filename, strerror(errno));
		return NULL;
	}

	while ((read = getline(&line, &len, f)) != -1) {
		if (read <= 1) continue;

		first_char = 0;
		while (first_char < read && (line[first_char] == ' ' || line[first_char] == '\t'))
			first_char++;

		if (line[first_char] == '#') continue;

		if (_read_property(line, read, &key, &value) == -1)
			continue;

		prop = (struct _property*) malloc(sizeof(struct _property));
		prop->key = key;
		prop->value = value;
		prop->next = NULL;
		if (config->size == 0)
			config->head = prop;
		else
			cur->next = prop;
		cur = prop;
		config->size++;
	}
	fclose(f);

	free(line);

	return config;
}

int config_to_stream(struct configuration *config, FILE *f) {
	struct _property *current = config->head;

	for (int i = 0; i < config->size; i++) {
		fprintf(f, "%s = %s\n", current->key, current->value);
		current = current->next;
	}

	return 0;
}

int config_to_file(struct configuration *config, char *filename) {
	FILE *f;
	if ((f = fopen(filename, "w")) == NULL) {
		fprintf(stderr, "write_config: cannot open file %s: %s\n", filename, strerror(errno));
		return -1;
	}

	config_to_stream(config, f);
	fclose(f);

	return 0;
}

struct configuration *get_sub_config(struct configuration *config, char *prefix) {
	int len = strlen(prefix);
	struct _property *prop, *cur, *cur_sub;
	struct configuration *sub = (struct configuration*) malloc(sizeof(struct configuration));
	sub->size = 0;
	sub->head = NULL;

	for (int i = 0; i < config->size; i++) {
		if (i == 0)
			cur = config->head;
		else
			cur = cur->next;

		if (strncmp(cur->key, prefix, len) != 0)
			continue;

		prop = (struct _property*) malloc(sizeof(struct _property));
		prop->key = cur->key + len;
		prop->value = cur->value;
		prop->next = NULL;
		while (prop->key[0] == '.') prop->key++;

		if (sub->head == NULL)
			sub->head = prop;
		else
			cur_sub->next = prop;
		cur_sub = prop;
		sub->size++;
	}

	return sub;
}

int get_config_value(struct configuration *config, char *key, char **value) {
	struct _property *prop = config->head;

	for (int i = 0; i < config->size; i++) {
		if (strcmp(key, prop->key) == 0) {
			*value = prop->value;
			return CONF_OK;
		}
		prop = prop->next;
	}

	return CONF_ERR;
}

int get_config_value_err(struct configuration *config, char *key, char **value) {
	if (get_config_value(config, key, value) == CONF_OK)
		return CONF_OK;

	fprintf(stderr, "Missing property %s\n", key);
	return CONF_ERR;
}

int get_config_integer(struct configuration *config, char *key, int *i) {
	char *buf;

	if (get_config_value_err(config, key, &buf) == CONF_ERR)
		return CONF_ERR;

	if (sscanf(buf, "%d", i) != 1) {
		fprintf(stderr, "%s expected an integer, got %s\n", key, buf);
		return CONF_ERR;
	}

	return CONF_OK;
}

int get_config_long(struct configuration *config, char *key, long *l) {
	char *buf;

	if (get_config_value_err(config, key, &buf) == CONF_ERR)
		return CONF_ERR;

	if (sscanf(buf, "%ld", l) != 1) {
		fprintf(stderr, "%s expected a long, got %s\n", key, buf);
		return CONF_ERR;
	}

	return CONF_OK;
}

int get_config_double(struct configuration *config, char *key, double *d) {
	char *buf;

	if (get_config_value_err(config, key, &buf) == CONF_ERR)
		return CONF_ERR;

	if (sscanf(buf, "%lf", d) != 1) {
		fprintf(stderr, "%s expected a double, got %s\n", key, buf);
		return CONF_ERR;
	}

	return CONF_OK;
}

int get_config_string(struct configuration *config, char *key, char **value) {
	char *buf, *val;
	int len;

	if (get_config_value_err(config, key, &val) == CONF_ERR)
		return CONF_ERR;
	len = strlen(val);
	buf = (char*) malloc((len + 1) * sizeof(char));
	memcpy(buf, val, len);
	buf[len] = '\0';

	*value = buf;
	return CONF_OK;
}

int get_config_string_array(struct configuration *config, char *key, int len, char ***a) {
	char *buf, *tok, *val;
	char **array;
	int val_len;

	if (get_config_value_err(config, key, &val) == CONF_ERR)
		return CONF_ERR;
	val_len = strlen(val);
	buf = (char*) malloc((val_len + 1) * sizeof(char));
	memcpy(buf, val, val_len);
	buf[val_len] = '\0';

	array = (char**) malloc(len * sizeof(char*));
	tok = strtok(buf, " \t");
	for (int i = 0; i < len; i++) {
		if (tok == NULL) {
			fprintf(stderr, "%s expected %d strings, found only %d\n", key, len, i);
			return CONF_ERR;
		}
		array[i] = tok;
		tok = strtok(NULL, " \t");
	}

	*a = array;
	return CONF_OK;
}

int get_config_integer_array(struct configuration *config, char *key, int len, int **a) {
	char *buf, *tok, *val;
	int *array, val_len;

	if (get_config_value_err(config, key, &val) == CONF_ERR)
		return CONF_ERR;
	val_len = strlen(val);
	buf = (char*) malloc((val_len + 1) * sizeof(char));
	memcpy(buf, val, val_len);
	buf[val_len] = '\0';

	array = (int*) malloc(len * sizeof(int));
	tok = strtok(buf, " \t");
	for (int i = 0; i < len; i++) {
		if (tok == NULL) {
			fprintf(stderr, "%s expected %d integers, found only %d\n", key, len, i);
			return CONF_ERR;
		}
		if (sscanf(tok, "%d", array + i) != 1) {
			fprintf(stderr, "%s expected an integer array, found %s\n", key, tok);
			return CONF_ERR;
		}

		tok = strtok(NULL, " \t");
	}

	free(buf);

	*a = array;
	return CONF_OK;
}

int get_config_long_array(struct configuration *config, char *key, int len, long **a) {
	char *buf, *tok, *val;
	long *array;
	int val_len;

	if (get_config_value_err(config, key, &val) == CONF_ERR)
		return CONF_ERR;
	val_len = strlen(val);
	buf = (char*) malloc((val_len + 1) * sizeof(char));
	memcpy(buf, val, val_len);
	buf[val_len] = '\0';

	array = (long*) malloc(len * sizeof(long));
	tok = strtok(buf, " \t");
	for (int i = 0; i < len; i++) {
		if (tok == NULL) {
			fprintf(stderr, "%s expected %d longs, found only %d\n", key, len, i);
			return CONF_ERR;
		}
		if (sscanf(tok, "%ld", array + i) != 1) {
			fprintf(stderr, "%s expected a long array, found %s\n", key, tok);
			return CONF_ERR;
		}

		tok = strtok(NULL, " \t");
	}

	free(buf);

	*a = array;
	return CONF_OK;
}

int get_config_double_array(struct configuration *config, char *key, int len, double **a) {
	char *buf, *tok, *val;
	double *array;
	int val_len;

	if (get_config_value_err(config, key, &val) == CONF_ERR)
		return CONF_ERR;
	val_len = strlen(val);
	buf = (char*) malloc((val_len + 1) * sizeof(char));
	memcpy(buf, val, val_len);
	buf[val_len] = '\0';

	array = (double*) malloc(len * sizeof(double));
	tok = strtok(buf, " \t");
	for (int i = 0; i < len; i++) {
		if (tok == NULL) {
			fprintf(stderr, "%s expected %d doubles, found only %d\n", key, len, i);
			return CONF_ERR;
		}
		if (sscanf(tok, "%lf", array + i) != 1) {
			fprintf(stderr, "%s expected a double array, found %s\n", key, tok);
			return CONF_ERR;
		}

		tok = strtok(NULL, " \t");
	}

	free(buf);

	*a = array;
	return CONF_OK;
}

void free_config(struct configuration *config) {
	struct _property *p;

	for (int i = 0; i < config->size; i++) {
		p = config->head;
		if (p == NULL) {
			fprintf(stderr, "Unexpectedly reached NULL in linked list at index %d/%d\n", i, config->size - 1);
			break;
		}
		config->head = p->next;
		free(p->key);
		free(p->value);
		free(p);
	}

	free(config);
}
